importScripts('/assets/cache-poyfill.js');

const filesToCache = [
    '/assets/img/finicial.jpg',
    '/assets/img/fprimaria.jpg',
    '/assets/img/fsecundaria.jpg',
    '/assets/img/volantesS.jpg',
    '/assets/img/volantesI.jpg',
    '/assets/img/escudo.png',
    'http://cdn.jsdelivr.net/npm/sweetalert2@11',
    'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js'
];

const staticCacheName = 'pages-cache-v1';

self.addEventListener('install', event => {
    console.log('Attempting to install service worker and cache static assets');
    event.waitUntil(
        caches.open(staticCacheName)
            .then(cache => {
                return cache.addAll(filesToCache);
            })
    );
});

self.addEventListener('fetch', event => {
    event.respondWith(
        caches.match(event.request)
            .then(response => {
                if (response) {
                    return response;
                }
                return fetch(event.request).catch( error => {
                    console.log('Not load ', event.request.url);
                });
                // TODO 4 - Add fetched files to the cache
            }).catch(error => {
                // TODO 6 - Respond with custom offline page
            })
    );
});
