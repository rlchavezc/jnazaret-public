<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Home extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * http://example.com/index/welcome
     * - or -
     * http://example.com/index/welcome/index
     * - or -
     * Since this controller is set as the default controller in
     * config/routes, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index/welcome/<method_name>
     *
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
    }
    /*
	 * Pages response
	 */
    public function index()
    {
        $data_header['title'] = 'Colegio Nazareth';
        $data_footer['sys_name'] = 'Colegio Nazareth';
        $this->load->view('common/header_common_view', $data_header);
        $this->load->view('home/index_view');
        $this->load->view('common/footer_common_view', $data_footer);
    }
    public function bienvenida()
    {
        $data_header['title'] = 'Colegio Nazareth';
        $data_footer['sys_name'] = 'Colegio Nazareth';
        $this->load->view('common/header_common_view', $data_header);
        $this->load->view('home/bienvenida_view');
        $this->load->view('common/footer_common_view', $data_footer);
    }
    public function quienes_somos($section = FALSE)
    {
        $data_header['title'] = 'Colegio Nazareth';
        $data_footer['sys_name'] = 'Colegio Nazareth';
        $data_body['section'] = $section;
        $this->load->view('common/header_common_view', $data_header);
        $this->load->view('home/quienes_somos_view', $data_body);
        $this->load->view('common/footer_common_view', $data_footer);
    }
    public function admision()
    {
        $data_header['title'] = 'Colegio Nazareth';
        $data_footer['sys_name'] = 'Colegio Nazareth';
        $this->load->view('common/header_common_view', $data_header);
        $this->load->view('home/admision_view');
        $this->load->view('common/footer_common_view', $data_footer);
    }
    public function actividades()
    {
        $data_header['title'] = 'Colegio Nazareth';
        $data_footer['sys_name'] = 'Colegio Nazareth';
        $this->load->view('common/header_common_view', $data_header);
        $this->load->view('home/actividades_view');
        $this->load->view('common/footer_common_view', $data_footer);
    }
    public function galeria()
    {
        $this->load->library('image_CRUD');
        $this->load->database();
        $image_crud = new image_CRUD();
        $image_crud->set_primary_key_field('id');
        $image_crud->set_url_field('url');
        $image_crud->set_table('gallery')
            ->set_image_path('assets/uploads/gallery');

        $image_crud->unset_delete();
        $image_crud->unset_upload();

        $output = $image_crud->render();

        $data_header['title'] = 'Colegio Nazareth';
        $data_footer['sys_name'] = 'Colegio Nazareth';

        $this->load->view('common/header_common_view', $data_header);
        $this->load->view('home/galeria_view', $output);
        $this->load->view('common/footer_common_view', $data_footer);
    }
    public function contactenos()
    {
        $data_header['title'] = 'Colegio Nazareth';
        $data_footer['sys_name'] = 'Colegio Nazareth';
        $this->load->view('common/header_common_view', $data_header);
        $this->load->view('home/contactenos_view');
        $this->load->view('common/footer_common_view', $data_footer);
    }
    public function exalumnos()
    {
        $data_header['title'] = 'Colegio Nazareth';
        $data_footer['sys_name'] = 'Colegio Nazareth';

        $this->load->library('image_CRUD');
        $this->load->database();
        $image_crud = new image_CRUD();
        $image_crud->set_primary_key_field('id');
        $image_crud->set_url_field('url');
        $image_crud->set_table('exgallery')
            ->set_image_path('assets/uploads/exgallery');
        $image_crud->unset_delete();
        $image_crud->unset_upload();

        $output = $image_crud->render();

        $this->load->view('common/header_common_view', $data_header);
        $this->load->view('home/exalumnos_view', array('output' => $output));
        $this->load->view('common/footer_common_view', $data_footer);
    }
    public function inicial()
    {
        $data_header['title'] = 'Colegio Nazareth';
        $data_footer['sys_name'] = 'Colegio Nazareth';
        $this->load->view('common/header_common_view', $data_header);
        $this->load->view('home/nivel/inicial_view');
        $this->load->view('common/footer_common_view', $data_footer);
    }
    public function primaria()
    {
        $data_header['title'] = 'Colegio Nazareth';
        $data_footer['sys_name'] = 'Colegio Nazareth';
        $this->load->view('common/header_common_view', $data_header);
        $this->load->view('home/nivel/primaria_view');
        $this->load->view('common/footer_common_view', $data_footer);
    }
    public function secundaria()
    {
        $data_header['title'] = 'Colegio Nazareth';
        $data_footer['sys_name'] = 'Colegio Nazareth';
        $this->load->view('common/header_common_view', $data_header);
        $this->load->view('home/nivel/secundaria_view');
        $this->load->view('common/footer_common_view', $data_footer);
    }
    public function libro_reclamaciones()
    {
        $data_header['title'] = 'Colegio Nazareth';
        $data_footer['sys_name'] = 'Colegio Nazareth';
        $this->load->view('common/header_common_view', $data_header);
        $this->load->view('home/libro_reclamaciones_view');
        $this->load->view('common/footer_common_view', $data_footer);
    }
    public function conciliacion()
    {
        $data_header['title'] = 'Colegio Nazareth';
        $data_footer['sys_name'] = 'Colegio Nazareth';
        $this->load->view('common/header_common_view', $data_header);
        $this->load->view('home/conciliacion_view');
        $this->load->view('common/footer_common_view', $data_footer);
    }
    /*
	 * Data response
	 */
    public function data_content($category = FALSE)
    {
        $this->setHeaderJson();
        $this->load->model('content_model');
        $data = $this->content_model->get_content($category);
        echo json_encode($data);
    }
    public function contact_mail()
    {
        $data = json_decode(trim(file_get_contents('php://input')), true);
        $this->setHeaderJson();
        $this->load->model('contact_model');
        $this->load->model('content_model');
        $this->contact_model->add_contact($data);
        $header = "FROM: Contacto de " . $data["name"];
        $body = "Nombre: " . $data["name"] . " \nCorreo: " . $data["email"] . " \nTelefono: " . $data["phone"] . "  \nComentarios: " . $data["comment"];
        $mail_admin = $this->content_model->get_content('mail_admin');
        $correo = $mail_admin['value'];
        $response = mail($correo, "Contactenos desde la Web", $body, $header);
        echo json_encode(array(
            'response' => $response
        ));
    }
    public function add_exalumno()
    {
        $data = json_decode(trim(file_get_contents('php://input')), true);
        $this->setHeaderJson();
        $this->load->model('exalumno_model');
        $this->load->model('content_model');
        $this->exalumno_model->add_exalumno($data);
        $header = "FROM: Nuevo registro de ex-alumno " . $data["name"];
        $body = "Nombre: " . $data["name"] . " " . $data["last_name"] . " \nCorreo: " . $data["email"] . " \n Ver detalles en: " . base_url() . "login";
        $mail_admin = $this->content_model->get_content('mail_admin');
        $correo = $mail_admin['value'];
        $response = mail($correo, "Envio desde la Web", $body, $header);
        echo json_encode(array(
            'response' => $response
        ));
    }
    public function add_libro_reclamacion()
    {
        $data = json_decode(trim(file_get_contents('php://input')), true);
        unset($data['leido']);
        $this->setHeaderJson();
        $this->load->model('libroreclamacion_model');
        $this->load->model('content_model');
        $mail_admin = $this->content_model->get_content('mail_admin');
        $response = $this->libroreclamacion_model->add_libro_reclamacion($data);
        try {
            $this->load->config('email');
            $this->load->library('email');
            $this->load->helper('file');
            $url = base_url() . "home/pdf_libro/{$response}";
            $body = file_get_contents('./assets/templates/mail_reclamacion.html');
            $body = str_replace("{{id}}", $response, $body);
            $body = str_replace("{{nombres}}", $data['nombres'], $body);
            $body = str_replace("{{primer_apellido}}", $data['primer_apellido'], $body);
            $body = str_replace("{{segundo_apellido}}", $data['segundo_apellido'], $body);
            $body = str_replace("{{tipo_documento}}", $data['tipo_documento'], $body);
            $body = str_replace("{{numero_documento}}", $data['numero_documento'], $body);
            $body = str_replace("{{celular}}", $data['celular'], $body);
            $body = str_replace("{{correo}}", $data['correo'], $body);
            $body = str_replace("{{tipo_reclamo}}", $data['tipo_reclamo'], $body);
            $body = str_replace("{{descripcion}}", $data['descripcion'], $body);
            $body = str_replace("{{url}}", $url, $body);
            $header = "Nuevo registro de libro de reclamacion de {$data["nombres"]} {$data["primer_apellido"]} {$data["segundo_apellido"]}";
            $correo = $mail_admin['value'];
            $from = $this->config->item('smtp_user');
            if (env('ENABLE_EMAIL', true)) {
                $this->email->set_newline("\r\n");
                $this->email->from($from, 'IEP Jesus de Nazareth');
                $this->email->to($data['correo']);
                $this->email->bcc($correo);
                $this->email->subject($header, 'IEP Jesus de Nazareth');
                $this->email->message($body);
                if ($this->email->send()) {
                    log_message('info', "mail sended to {$data['correo']}");
                } else {
                    log_message('error', $this->email->print_debugger());
                }
            }
        } catch (\Throwable $th) {
            http_response_code(400);
            log_message('error', $th->getMessage());
        }
        echo json_encode(array(
            'response' => $response
        ));
    }
    public function add_conciliacion()
    {
        $data = json_decode(trim(file_get_contents('php://input')), true);
        unset($data['aceptacion_declaracion']);
        unset($data['aceptacion_solucion']);
        unset($data['validado']);
        $this->setHeaderJson();
        $this->load->model('conciliacion_model');
        $this->load->model('libroreclamacion_model');
        $this->load->model('content_model');
        $mail_admin = $this->content_model->get_content('mail_admin');
        $response = $this->conciliacion_model->add_conciliacion($data);
        $response_libro = $this->libroreclamacion_model->update_libro_reclamacion(
            $data['id_reclamo'],
            [
                'estado' => $this->libroreclamacion_model::CONCILIADO
            ]
        );
        try {
            $this->load->config('email');
            $this->load->library('email');
            $header = "Nuevo registro de conciliacion del reclamo: " . $data['id_reclamo'];
            $body = "Se ha conciliado la siguiente hoja de reclamación correctamente: \nId de reclamo: {$data['id_reclamo']} de {$data['correo']}\n Colegio Privado Jesus de Nazareth";
            $correo = $mail_admin['value'];
            $from = $this->config->item('smtp_user');
            if (env('ENABLE_EMAIL', true)) {
                $this->email->set_newline("\r\n");
                $this->email->from($from, 'IEP Jesus de Nazareth');
                $this->email->to($correo);
                $this->email->bcc($data["correo"]);
                $this->email->subject($header);
                $this->email->message($body);
                if ($this->email->send()) {
                    log_message('info', "mail sended to {$data['correo']}");
                } else {
                    log_message('error', $this->email->print_debugger());
                }
            }
        } catch (\Throwable $th) {
            http_response_code(400);
            log_message('error', $th->getMessage());
        }
        echo json_encode(array(
            'response' => $response
        ));
    }
    public function data_reclamo_registrado($id_reclamo)
    {
        $this->setHeaderJson();
        $this->load->model('libroreclamacion_model');
        $data = $this->libroreclamacion_model->get_libro_reclamacion($id_reclamo);
        if ($data && $data['estado'] == $this->libroreclamacion_model::REGISTRADO) {
            echo json_encode(['nombres' => $data['nombres'], 'correo' => $data['correo']]);
            die();
        }
        http_response_code(400);
        if ($data && $data['estado'] == $this->libroreclamacion_model::CONCILIADO) {
            echo json_encode(['error' => 'El id del reclamo ya fue conciliado']);
            die();
        }
        echo json_encode(['error' => 'El id del reclamo no existe']);
    }
    public function pdf_libro($id_reclamo = null)
    {
        $this->load->library('pdfgenerator');
        $filename = 'comprobante_libro_reclamacion';
        $body = file_get_contents('./assets/templates/pdf_libro_reclmacion.html');
        if ($id_reclamo) {
            $this->load->model('libroreclamacion_model');
            $img = file_get_contents('./assets/img/escudo.png');
            $img = base64_encode($img);
            $img = "data:image/png;base64,{$img}";

            $data = $this->libroreclamacion_model->get_libro_reclamacion($id_reclamo);
            $body = str_replace("{{id}}", $id_reclamo, $body);
            $body = str_replace("{{nombres}}", $data['nombres'], $body);
            $body = str_replace("{{primer_apellido}}", $data['primer_apellido'], $body);
            $body = str_replace("{{segundo_apellido}}", $data['segundo_apellido'], $body);
            $body = str_replace("{{tipo_documento}}", $data['tipo_documento'], $body);
            $body = str_replace("{{numero_documento}}", $data['numero_documento'], $body);
            $body = str_replace("{{celular}}", $data['celular'], $body);
            $body = str_replace("{{correo}}", $data['correo'], $body);
            $body = str_replace("{{tipo_reclamo}}", $data['tipo_reclamo'], $body);
            $body = str_replace("{{descripcion}}", $data['descripcion'], $body);
            $body = str_replace("{{img}}", $img, $body);
        }
        $this->pdfgenerator->generate($body, $filename, true);
    }
    private function setHeaderJson()
    {
        header('Content-Type: application/json');
    }
}
