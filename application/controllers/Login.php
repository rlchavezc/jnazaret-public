<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Login extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * http://example.com/index.php/welcome
     * - or -
     * http://example.com/index.php/welcome/index
     * - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     *
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->library('session');
    }
    public function index()
    {
        $this->session->sess_destroy();
        $data_header['title'] = 'Admin Jnazareth';
        $data_nav['title'] = 'Invitado';
        $data_login['sys_name'] = 'Admin Jnazareth';
        $data_footer['sys_name'] = 'Admin Jnazareth';
        $this->load->view('common/header_common_view', $data_header);
        $this->load->view('common/nav_common_view', $data_nav);
        $this->load->view('login_view', $data_login);
        $this->load->view('common/footer_common_view', $data_footer);
    }
    public function do_login()
    {
        // admin - jn2015admin
        $data = json_decode(trim(file_get_contents('php://input')), true);
        $login_result = $this->user_model->login($data['user'], sha1($data['pass']));
        header('Content-Type: application/json');
        if ($login_result) {
            $this->session->set_userdata($login_result);
        } else {
            $this->session->sess_destroy();
            http_response_code(400);
            echo json_encode(array(
                'error' => 'Login incorrecto'
            ));
            die();
        }
        echo json_encode(array(
            'login' => $login_result,
            'sha1' => sha1($login_result['pass'])
        ));
    }
    public function logout()
    {
        $this->session->sess_destroy();
        header('Location: ' . base_url() . 'login');
        die();
    }
}
