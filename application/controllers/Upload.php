<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * @property Contact_model $contact_model
 * @property Content_model $content_model
 * @property Libroreclamacion_model $libroreclamacion_model
 */
class Upload extends CI_Controller
{
    private $default_config;
    private const ADJUNTO1 = 'adjunto1';
    private const ADJUNTO2 = 'adjunto2';

    private const FOTO1 = 'foto1';
    private const FOTO2 = 'foto2';
    private const FIRMA = 'firma';

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->default_config = [];
        $this->default_config['upload_path']          = './assets/uploads/';
        $this->default_config['allowed_types']        = 'gif|jpg|png|pdf|docx|doc|xls|xlsx';
        $this->default_config['max_size']             = 4096;
        $this->default_config['max_width']            = 2048;
        $this->default_config['max_height']           = 2048;
    }

    public function do_upload_conciliacion($type)
    {
        $this->setHeaderJson();
        if (!$this->validateTypeConciliacion($type)) {
            http_response_code(400);
            echo json_encode(array('error' => 'Tipo inválido'));
            die();
        }
        $config = $this->default_config;
        $this->load->helper('util_helper');
        $config['upload_path'] = conciliacion_location_file($type);
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($type)) {
            $error = array('error' => $this->upload->display_errors());
            http_response_code(400);
            echo json_encode($error);
        } else {
            $data = $this->upload->data();
            echo json_encode(['file_name' => $data['file_name']]);
        }
    }

    public function do_upload_libro($type)
    {
        $this->setHeaderJson();
        if (!$this->validateTypeLibro($type)) {
            http_response_code(400);
            echo json_encode(array('error' => 'Tipo inválido'));
            die();
        }
        $config = $this->default_config;
        $this->load->helper('util_helper');
        $config['upload_path'] = conciliacion_location_file($type);
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($type)) {
            $error = array('error' => $this->upload->display_errors());
            http_response_code(400);
            echo json_encode($error);
        } else {
            $data = $this->upload->data();
            echo json_encode(['file_name' => $data['file_name']]);
        }
    }

    private function setHeaderJson()
    {
        header('Content-Type: application/json');
    }

    private function validateTypeConciliacion($type)
    {
        return $type === self::FOTO1 || $type === self::FOTO2 || $type === self::FIRMA;
    }

    private function validateTypeLibro($type)
    {
        return $type === self::ADJUNTO1 || $type === self::ADJUNTO2;
    }
}
