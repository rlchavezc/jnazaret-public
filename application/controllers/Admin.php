<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * @property Contact_model $contact_model
 * @property Content_model $content_model
 * @property Libroreclamacion_model $libroreclamacion_model
 */
class Admin extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * http://example.com/index/welcome
     * - or -
     * http://example.com/index/welcome/index
     * - or -
     * Since this controller is set as the default controller in
     * config/routes, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index/welcome/<method_name>
     *
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('content_model');
        $this->load->library('session');

        if (!$this->session->has_userdata('name')) {
            header('Location: ' . base_url() . 'login');
            die();
        }
    }
    /*
	 * Pages response
	 */
    public function index()
    {
        $data_header['title'] = 'Colegio Nazareth';
        $data_footer['sys_name'] = 'Colegio Nazareth';
        $data_nav['title'] = 'Admin';
        $this->load->view('common/header_common_view', $data_header);
        $this->load->view('common/nav_common_view', $data_nav);
        $this->load->view('admin/admin_view');
        $this->load->view('common/footer_common_view', $data_footer);
    }
    public function contact()
    {
        $data_header['title'] = 'Colegio Nazareth';
        $data_footer['sys_name'] = 'Colegio Nazareth';
        $data_nav['title'] = 'Admin';
        $this->load->view('common/header_common_view', $data_header);
        $this->load->view('common/nav_common_view', $data_nav);
        $this->load->view('admin/admin_contact_view');
        $this->load->view('common/footer_common_view', $data_footer);
    }
    public function exalumnos()
    {
        $data_header['title'] = 'Colegio Nazareth';
        $data_footer['sys_name'] = 'Colegio Nazareth';
        $data_nav['title'] = 'Admin';
        $this->load->view('common/header_common_view', $data_header);
        $this->load->view('common/nav_common_view', $data_nav);
        $this->load->view('admin/admin_exalumno_view');
        $this->load->view('common/footer_common_view', $data_footer);
    }
    public function galeria()
    {
        $data_header['title'] = 'Colegio Nazareth';
        $data_footer['sys_name'] = 'Colegio Nazareth';
        $data_nav['title'] = 'Admin';

        $this->load->library('image_CRUD');
        $this->load->database();
        $image_crud = new image_CRUD();
        $image_crud->set_primary_key_field('id');
        $image_crud->set_url_field('url');
        $image_crud->set_title_field('title');
        $image_crud->set_table('gallery')
            ->set_ordering_field('priority')
            ->set_image_path('assets/uploads/gallery');
        $output = $image_crud->render();

        $this->load->view('common/header_common_view', $data_header);
        $this->load->view('common/nav_common_view', $data_nav);
        $this->load->view('admin/admin_gallery_view', $output);
        $this->load->view('common/footer_common_view', $data_footer);
    }
    public function galeria_ex()
    {
        $data_header['title'] = 'Colegio Nazareth';
        $data_footer['sys_name'] = 'Colegio Nazareth';
        $data_nav['title'] = 'Admin';

        $this->load->library('image_CRUD');
        $this->load->database();
        $image_crud = new image_CRUD();
        $image_crud->set_primary_key_field('id');
        $image_crud->set_url_field('url');
        $image_crud->set_title_field('title');
        $image_crud->set_table('exgallery')
            ->set_ordering_field('priority')
            ->set_image_path('assets/uploads/exgallery');
        $output = $image_crud->render();

        $this->load->view('common/header_common_view', $data_header);
        $this->load->view('common/nav_common_view', $data_nav);
        $this->load->view('admin/admin_gallery_ex_view', $output);
        $this->load->view('common/footer_common_view', $data_footer);
    }
    public function libro_reclamaciones()
    {
        $data_header['title'] = 'Colegio Nazareth';
        $data_footer['sys_name'] = 'Colegio Nazareth';
        $data_nav['title'] = 'Admin';
        $this->load->view('common/header_common_view', $data_header);
        $this->load->view('common/nav_common_view', $data_nav);
        $this->load->view('admin/admin_libro_reclamacion_view');
        $this->load->view('common/footer_common_view', $data_footer);
    }
    public function conciliacion()
    {
        $data_header['title'] = 'Colegio Nazareth';
        $data_footer['sys_name'] = 'Colegio Nazareth';
        $data_nav['title'] = 'Admin';
        $this->load->view('common/header_common_view', $data_header);
        $this->load->view('common/nav_common_view', $data_nav);
        $this->load->view('admin/admin_conciliacion_view');
        $this->load->view('common/footer_common_view', $data_footer);
    }
    /*
	 * Data response
	 */
    public function sidebar($index = 0)
    {
        header('Content-Type: application/json');
        $data = array(
            array(
                'label' => 'Principal',
                'url' => base_url() . 'admin/index',
                'active' => 0
            ),
            array(
                'label' => 'Contactos',
                'url' => base_url() . 'admin/contact',
                'active' => 0
            ),
            array(
                'label' => 'Exalumnos',
                'url' => base_url() . 'admin/exalumnos',
                'active' => 0
            ),
            array(
                'label' => 'Galería',
                'url' => base_url() . 'admin/galeria',
                'active' => 0
            ),
            array(
                'label' => 'Galería exalumnos',
                'url' => base_url() . 'admin/galeria_ex',
                'active' => 0
            ),
            array(
                'label' => 'Listado de reclamos',
                'url' => base_url() . 'admin/libro_reclamaciones',
                'active' => 0
            ),
            array(
                'label' => 'Conciliar reclamo',
                'url' => base_url() . 'admin/conciliacion',
                'active' => 0
            )
        );
        $data[min(array(
            count($data) - 1,
            $index
        ))]['active'] = 1;
        echo json_encode($data);
    }
    public function data_content($category = FALSE)
    {
        header('Content-Type: application/json');
        $this->load->model('content_model');
        $data = $this->content_model->get_content($category);
        echo json_encode($data);
    }
    public function data_contact($id = FALSE)
    {
        header('Content-Type: application/json');
        $this->load->model('contact_model');
        $data = $this->contact_model->get_contact($id);
        echo json_encode($data);
    }
    public function data_exalumno($id = FALSE)
    {
        header('Content-Type: application/json');
        $this->load->model('exalumno_model');
        $data = $this->exalumno_model->get_exalumno($id);
        echo json_encode($data);
    }
    public function data_libro_reclamaciones($id = FALSE)
    {
        header('Content-Type: application/json');
        $this->load->model('libroreclamacion_model');
        $data = $this->libroreclamacion_model->get_libro_reclamacion($id);
        echo json_encode($data);
    }
    public function update_content()
    {
        $data = $this->input->post();
        if (empty($data)) {
            $data = json_decode(trim(file_get_contents('php://input')), true);
        }
        $result = $this->content_model->update_content($data['category'], $data);
        header('Content-Type: application/json');
        echo json_encode($result);
    }
    public function delete_contact($id)
    {
        $this->load->model('contact_model');
        $result = $this->contact_model->delete_contact($id);
        header('Content-Type: application/json');
        echo json_encode($result);
    }
    public function delete_exalumno($id)
    {
        $this->load->model('exalumno_model');
        $result = $this->exalumno_model->delete_exalumnos($id);
        header('Content-Type: application/json');
        echo json_encode($result);
    }
}
