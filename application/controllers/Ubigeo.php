<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * @property Contact_model $contact_model
 * @property Content_model $content_model
 * @property Libroreclamacion_model $libroreclamacion_model
 * @property Ubigeo_model $ubigeo_model
 */
class Ubigeo extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_departamentos()
    {
        $this->setHeaderJson();
        $this->load->model('ubigeo_model');
        $data = $this->ubigeo_model->get_departamentos();
        echo json_encode($data);
    }

    public function get_provincias($departamento)
    {
        $this->setHeaderJson();
        $this->load->model('ubigeo_model');
        $departamento = urldecode($departamento);
        $data = $this->ubigeo_model->get_provincias($departamento);
        echo json_encode($data);
    }

    public function get_distritos($departamento, $provincia)
    {
        $this->setHeaderJson();
        $this->load->model('ubigeo_model');
        $departamento = urldecode($departamento);
        $provincia = urldecode($provincia);
        $data = $this->ubigeo_model->get_distritos($departamento, $provincia);
        echo json_encode($data);
    }

    private function setHeaderJson()
    {
        header('Content-Type: application/json');
    }
}
