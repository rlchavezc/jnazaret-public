<?php

use Dompdf\Dompdf;
use Dompdf\Options;

defined('BASEPATH') or exit('No direct script access allowed');
// Al requerir el autoload, cargamos todo lo necesario para trabajar
// require_once APPPATH."/third_party/dompdf/autoload.inc.php";

class Pdfgenerator
{
    // por defecto, usaremos papel A4 en vertical, salvo que digamos otra cosa al momento de generar un PDF
    public function generate($html, $filename = '', $stream = TRUE, $paper = 'A4', $orientation = "portrait")
    {
        $options = new Options();
        $options->set('isRemoteEnabled',true);
        $dompdf = new Dompdf($options);
        $dompdf->loadHtml($html);
        $dompdf->setPaper($paper, $orientation);
        $dompdf->render();
        $dompdf->stream("{$filename}.pdf", array("Attachment" => $stream));
    }
}
