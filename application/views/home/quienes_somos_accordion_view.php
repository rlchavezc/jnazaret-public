<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
?>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	<div class="panel panel-primary">
		<div class="panel-heading" role="tab" id="headingMision">
			<h4 class="panel-title">
				<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseMision" aria-expanded="true" aria-controls="collapseMision">
					{{mision_content.title}}
				</a>
			</h4>
		</div>
		<div id="collapseMision" class="panel-collapse collapse"
			role="tabpanel" aria-labelledby="headingMision">
			<div class="panel-body" ng-bind-html="mision_content.value">
				<!-- 
				<blockquote>
					<p></p>
				</blockquote>
				<div class="center-block">
					<img src="<?php echo base_url()?>assets/img/mision.jpg" alt="Misión" class="img-responsive img-thumbnail" />
				</div>
				 -->
			</div>
		</div>
	</div>
	<div class="panel panel-primary">
		<div class="panel-heading" role="tab" id="headingVision">
			<h4 class="panel-title">
				<a class="collapsed" role="button" data-toggle="collapse"
					data-parent="#accordion" href="#collapseVision"
					aria-expanded="false" aria-controls="collapseVision">{{vision_content.title}}</a>
			</h4>
		</div>
		<div id="collapseVision" class="panel-collapse collapse"
			role="tabpanel" aria-labelledby="headingVision">
			<div class="panel-body" ng-bind-html="vision_content.value">
				<!-- 
				<blockquote>
					<p></p>
				</blockquote>
				<div class="center-block">
					<img src="<?php echo base_url()?>assets/img/vision.jpg" alt="Visión" class="img-responsive img-thumbnail" />
				</div>
				 -->
			</div>
		</div>
	</div>
	<div class="panel panel-primary">
		<div class="panel-heading" role="tab" id="headingHistoria">
			<h4 class="panel-title">
				<a class="collapsed" role="button" data-toggle="collapse"
					data-parent="#accordion" href="#collapseHistoria"
					aria-expanded="false" aria-controls="collapseHistoria">{{historia_content.title}}</a>
			</h4>
		</div>
		<div id="collapseHistoria" class="panel-collapse collapse"
			role="tabpanel" aria-labelledby="headingHistoria">
			<div class="panel-body" ng-bind-html="historia_content.value">
			</div>
		</div>
	</div>
	<div class="panel panel-primary">
		<div class="panel-heading" role="tab" id="headingPerfil">
			<h4 class="panel-title">
				<a class="collapsed" role="button" data-toggle="collapse"
					data-parent="#accordion" href="#collapsePerfil"
					aria-expanded="false" aria-controls="collapsePerfil">{{perfil_content.title}}</a>
			</h4>
		</div>
		<div id="collapsePerfil" class="panel-collapse collapse"
			role="tabpanel" aria-labelledby="headingPerfil">
			<div class="panel-body" ng-bind-html="perfil_content.value">
			</div>
		</div>
	</div>
	<div class="panel panel-primary">
		<div class="panel-heading" role="tab" id="headingPropuesta">
			<h4 class="panel-title">
				<a class="collapsed" role="button" data-toggle="collapse"
					data-parent="#accordion" href="#collapsePropuesta"
					aria-expanded="false" aria-controls="collapsePropuesta">{{propuesta_content.title}}</a>
			</h4>
		</div>
		<div id="collapsePropuesta" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingPropuesta">
			<div class="panel-body" ng-bind-html="propuesta_content.value">
			</div>
		</div>
	</div>
	<div class="panel panel-primary">
		<div class="panel-heading" role="tab" id="headingColaborador">
			<h4 class="panel-title">
				<a class="collapsed" role="button" data-toggle="collapse"
					data-parent="#accordion" href="#collapseColaborador"
					aria-expanded="false" aria-controls="collapseColaborador">Colaboradores</a>
			</h4>
		</div>
		<div id="collapseColaborador" class="panel-collapse collapse"
			role="tabpanel" aria-labelledby="headingColaborador">
			<div class="panel-body" id="colaboradorAccordion">
				<div class="panel panel-primary">
					<div class="panel-heading" role="tab" id="headingAreaEducativa">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse"
								data-parent="#colaboradorAccordion" href="#collapseAreaEducativa"
								aria-expanded="false" aria-controls="collapseAreaEducativa">{{area_educativa_content.title}}</a>
						</h4>
					</div>
					<div id="collapseAreaEducativa" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingAreaEducativa">
						<div class="panel-body">
						</div>
						<div ng-bind-html="area_educativa_content.value"></div>
					</div>
				</div>
				<div class="panel panel-primary">
					<div class="panel-heading" role="tab" id="headingAreaAdministrativa">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse"
								data-parent="#colaboradorAccordion" href="#collapseAreaAdministrativa"
								aria-expanded="false" aria-controls="collapseAreaAdministrativa">{{area_administrativa_content.title}}</a>
						</h4>
					</div>
					<div id="collapseAreaAdministrativa" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingAreaAdministrativa">
						<div class="panel-body">
						</div>
						<div ng-bind-html="area_administrativa_content.value">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>