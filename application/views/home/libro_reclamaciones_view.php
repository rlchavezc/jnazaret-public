<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="container">
    <?php $this->load->view('home/common/home_header_view'); ?>
    <?php $this->load->view('common/slider_common_view', array('id' => 'main_carousel', 'ng_controller' => 'mainCarouselCtrl')); ?>
    <!-- Example row of columns -->
    <div class="row">
        <?php $this->load->view('home/common/left_panel_view'); ?>
        <div class="col-md-8">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h3>Libro de reclamaciones</h3>
                    </div>
                </div>
                <div class="panel-body" ng-controller="libroCtrl">
                    <h4>Hoja de reclamación</h4>
                    <p>Identificación del cosumidor reclamante. Su identificador de reclamo <span class="badge" ng-bind="id_reclamo"></span></p>
                    <p>* Primero ingrese la siguiente información.</p>
                    <form class="form-horizontal" name="libro_form">
                        <div class="form-group">
                            <label for="nombre" class="col-sm-2 control-label">Nombres</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="nombres" name="nombres" placeholder="Nombres" ng-model="libro.nombres" required="required" />
                                <span class="badge" ng-show="libro_form.nombres.$invalid && !libro_form.nombres.$pristine">El nombre es obligatorio</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nombre" class="col-sm-2 control-label">Primer apellido</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="primer_apellido" name="primer_pellido" placeholder="Primer apellido" ng-model="libro.primer_apellido" required="required" />
                                <span class="badge" ng-show="libro_form.primer_pellido.$invalid && !libro_form.primer_pellido.$pristine">El primer apellido es obligatorio</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nombre" class="col-sm-2 control-label">Segundo Apellido</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="segundo_apellido" name="segundo_apellido" placeholder="Segundo Apellido" ng-model="libro.segundo_apellido" required="required" />
                                <span class="badge" ng-show="libro_form.segundo_apellido.$invalid && !libro_form.segundo_apellido.$pristine">El segundo apellido es obligatorio</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nombre" class="col-sm-2 control-label">Tipo de documento</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="tipo_documento" name="tipo_documento" placeholder="Seleccione uno" ng-model="libro.tipo_documento" required="required">
                                    <optgroup label="Seleccione uno">
                                        <option value="DNI">DNI</option>
                                        <option value="CE">CE</option>
                                    </optgroup>
                                </select>
                                <span class="badge" ng-show="libro_form.tipo_documento.$invalid && !libro_form.tipo_documento.$pristine">El tipo de documento es obligatorio</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nombre" class="col-sm-2 control-label">Nro Documento</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="numero_documento" name="numero_documento" placeholder="Nro Documento" ng-model="libro.numero_documento" required="required" />
                                <span class="badge" ng-show="libro_form.numero_documento.$invalid && !libro_form.numero_documento.$pristine">El número de documento es obligatorio</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="telefono" class="col-sm-2 control-label">Celular</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="celular" name="celular" placeholder="Celular" ng-model="libro.celular" required="required" />
                                <span class="badge" ng-show="libro_form.celular.$invalid && !libro_form.celular.$pristine">El número de celular es obligatorio</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="telefono" class="col-sm-2 control-label">Departamento</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="departamento" id="departamento" ng-model="libro.departamento" ng-change="loadProvincias()" required="required" ng-disabled="waitDepartamentos">
                                    <option ng-repeat="option in departamentos" ng-value="option.departamento">{{option.departamento}}</option>
                                </select>
                                <span class="badge" ng-show="libro_form.departamento.$invalid && !libro_form.departamento.$pristine">El departamento es obligatorio</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="telefono" class="col-sm-2 control-label">Provincia</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="provincia" id="provincia" ng-model="libro.provincia" ng-change="loadDistritos()" required="required" ng-disabled="waitProvincias">
                                    <option ng-repeat="option in provincias" ng-value="option.provincia">{{option.provincia}}</option>
                                </select>
                                <span class="badge" ng-show="libro_form.provincia.$invalid && !libro_form.provincia.$pristine">La provincia es obligatoria</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="telefono" class="col-sm-2 control-label">Distrito</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="distrito" id="distrito" ng-model="libro.distrito" required="required" ng-disabled="waitDistritos">
                                    <option ng-repeat="option in distritos" ng-value="option.distrito">{{option.distrito}}</option>
                                </select>
                                <span class="badge" ng-show="libro_form.distrito.$invalid && !libro_form.distrito.$pristine">El distrito es obligatorio</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="telefono" class="col-sm-2 control-label">Dirección</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Dirección" ng-model="libro.direccion" required="required" />
                                <span class="badge" ng-show="libro_form.direccion.$invalid && !libro_form.direccion.$pristine">La dirección es obligatoria</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="telefono" class="col-sm-2 control-label">Referencia</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="referencia" name="referencia" placeholder="Referencia" ng-model="libro.referencia" required="required" />
                                <span class="badge" ng-show="libro_form.referencia.$invalid && !libro_form.referencia.$pristine">La referencia es obligatoria</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="correo" class="col-sm-2 control-label">Correo</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="correo" name="correo" placeholder="Correo" ng-model="libro.correo" required="required" />
                                <span class="badge" ng-show="libro_form.correo.$invalid && !libro_form.correo.$pristine">El correo es obligatorio</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="correo" class="col-sm-2 control-label">Es menor de edad?</label>
                            <div class="col-sm-10">
                                <div class="radio">
                                    <label><input type="radio" name="menor_edad" ng-model="libro.menor_edad" value="si">Sí</label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" name="menor_edad" ng-model="libro.menor_edad" value="no">No</label>
                                </div>
                                <span class="badge" ng-show="libro_form.menor_edad.$invalid && !libro_form.menor_edad.$pristine">Seleccione uno</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-11">
                                <p>Detalle de reclamo</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tipo_reclamo" class="col-sm-2 control-label">Tipo de reclamo</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="tipo_reclamo" name="tipo_reclamo" placeholder="Seleccione uno" ng-model="libro.tipo_reclamo">
                                    <optgroup label="Seleccione uno">
                                        <option value="Queja">Queja</option>
                                        <option value="Reclamacion">Reclamación</option>
                                    </optgroup>
                                </select>
                                <span class="badge" ng-show="libro_form.tipo_reclamo.$invalid && !libro_form.tipo_reclamo.$pristine">Seleccione uno</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tipo_reclamo" class="col-sm-2 control-label">Fecha de reclamo</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control" id="fecha_reclamo" name="fecha_reclamo" placeholder="Fecha de reclamo" ng-model="libro.fecha_reclamo" required="required" />
                                <span class="badge" ng-show="libro_form.fecha_reclamo.$invalid && !libro_form.fecha_reclamo.$pristine">La fecha de reclamo es obligatoria</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="comentarios" class="col-sm-2 control-label">Descripción del reclamo o queja</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="3" id="descripcion" name="descripcion" placeholder="Descripción del reclamo o queja" ng-model="libro.descripcion" required="required"></textarea>
                                <span class="badge" ng-show="libro_form.descripcion.$invalid && !libro_form.descripcion.$pristine">La descripción es obligatoria</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nombre" class="col-sm-2 control-label">Carga adjunto 1</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <input type="file" class="form-control" id="adjunto1" placeholder="Adjunto1" />
                                    <input type="hidden" name="adjunto1" ng-model="libro.adjunto1" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" ng-disabled="waitAdjunto1" ng-click="addFile('adjunto1')">Subir archivo</button>
                                        <button class="btn btn-info" ng-disabled="waitAdjunto1" ng-click="toggleAdjunto2()" ng-if="!showAdjunto2"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                        <button class="btn btn-info" ng-disabled="waitAdjunto1" ng-click="toggleAdjunto2()" ng-if="showAdjunto2"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                    </span>
                                </div>
                                <span class="badge" ng-show="libro_form.adjunto1.$invalid && !libro_form.adjunto1.$pristine">La foto es obligatoria</span>
                            </div>
                        </div>
                        <div class="form-group" ng-if="showAdjunto2">
                            <label for="nombre" class="col-sm-2 control-label">Cargar adjunto 2</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <input type="file" class="form-control" id="adjunto2" placeholder="Adjunto2" />
                                    <input type="hidden" name="adjunto2" ng-model="libro.adjunto2" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" ng-disabled="waitAdjunto2" ng-click="addFile('adjunto2')">Subir archivo</button>
                                    </span>
                                </div>
                                <span class="badge" ng-show="libro_form.adjunto2.$invalid && !libro_form.adjunto2.$pristine">La foto es obligatoria</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-11">
                                <p>(1) Reclamación: Desacuerdo relacionado con el servicio.</p>
                                <p>(2) Queja: Desacuerdo no relacionado con el servicio, o malestar o insatisfacción con la atencón al público.</p>
                                <ul>
                                    <li>La formulación del reclamo no excluye el recurso a otros medios de resolución de contraversias ni es un requisito previo para presentar una denuncia ante el indecopi.</li>
                                    <li>El proveedor deberá dar respuesta al reclamo en un plazo no mayor a treinta (30) días calendario, pudiendo extender el plazo hasta 30 días.</li>
                                    <li>Con la firma de este documento, el cliente autoriza a ser contactado después de la tramitación de la reclamación para evaluar la calidad y satisfacción del proceso de atención de reclamaciones.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-11">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="leido" ng-model="libro.leido" required>* Acepto el contenido de este formulario y la veracidad de los hechos descritos bajo Declaración Jurada.</label>
                                    <span class="badge" ng-show="libro_form.leido.$invalid && !libro_form.leido.$pristine">Debe aceptar los términos</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="btn-group">
                                    <ul class="hidden">
                                        <li ng-repeat="(key, errors) in libro_form.$error track by $index"> <strong>{{ key }}</strong> errors
                                            <ul>
                                                <li ng-repeat="e in errors">{{ e.$name }} has an error: <strong>{{ key }}</strong>.</li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <button type="submit" ng-disabled="!libro_form.$valid || waitReclamo" class="btn btn-primary" ng-click="sendLibroReclamacion()">Enviar hoja de reclamación</button>
                                    <button class="btn btn-default" ng-click="reset()">Reestablecer formulario</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--<?php $this->load->view('home/common/right_panel_view'); ?>-->
    </div>
</div>
<!-- /container -->
<script type="text/javascript">
    angular
        .module("mainApp", [])
        .factory("dataFactory", ['$http', '$q', '$location',
            function($http, $q, $location) {
                var dataBaseUrl = "<?php echo base_url(); ?>";
                var objFactory = {};
                objFactory.get = function(q) {
                    return $http.get(dataBaseUrl + q).then(function(data, status, headers, config) {
                        return data.data;
                    }, function(data, status, headers, config) {
                        console.log('[GET] ocurrió un error en ' + dataBaseUrl + q, data);
                    });
                };
                objFactory.post = function(q, object) {
                    return $http.post(dataBaseUrl + q, object).then(function(data, status, headers, config) {
                        return data.data;
                    }, function(data, status, headers, config) {
                        console.log('[POST] ocurrió un error en ' + dataBaseUrl + q, data);
                        console.log(object);
                    });
                };
                objFactory.postFile = function(file, q, type) {
                    var fd = new FormData();
                    fd.append(type, file);
                    return $http.post(dataBaseUrl + q, fd, {
                            transformRequest: angular.identity,
                            headers: {
                                'Content-Type': undefined,
                                'Process-Data': false
                            }
                        })
                        .then(function(data) {
                            return data.data;
                        }, function() {
                            console.log('[POST] ocurrió un error en ' + uploadUrl, file);
                            console.log(object);
                        });
                };
                return objFactory;
            }
        ])
        .controller("libroCtrl", function($scope, $sce, dataFactory) {
            $scope.id_reclamo = "####";
            $scope.waitAdjunto1 = false;
            $scope.waitAdjunto2 = false;
            $scope.waitReclamo = false;
            $scope.waitDepartamentos = false;
            $scope.waitProvincias = false;
            $scope.waitDistritos = false;
            $scope.showAdjunto2 = false;

            $scope.sendLibroReclamacion = function() {
                $scope.waitReclamo = true;
                $scope.libro.fecha_reclamo = moment($scope.libro.fecha_reclamo).format("YYYY-MM-DD HH:mm:ss");
                dataFactory.post('home/add_libro_reclamacion', $scope.libro).then(function(data, status, headers, config) {
                    $scope.waitReclamo = false;
                    $scope.response = data;
                    Swal.fire(
                        'Registro satisfactorio',
                        `Su identificador de reclamo es: ${data.response}`,
                        'success'
                    );
                    $scope.id_reclamo = data.response;
                    $scope.reset();
                }, function(error) {
                    $scope.waitReclamo = false;
                    Swal.fire(
                        'Error al registrar la hoja de reclamación',
                        error.error,
                        'error'
                    );
                });
            };
            $scope.addFile = function(id) {
                let file = document.getElementById(id).files[0],
                    fileReader = new FileReader();
                if (!file) {
                    Swal.fire(
                        'Advertencia!',
                        'Seleccione un archivo.',
                        'error'
                    );
                    return;
                }
                $scope.waitAdjunto1 = true;
                $scope.waitAdjunto2 = true;
                $scope.waitFirma = true;

                fileReader.onloadend = function(e) {
                    var src = e.target.result;
                    dataFactory.postFile(file, `upload/do_upload_libro/${id}`, id).then(function(data, status, headers, config) {
                        $scope.waitAdjunto1 = false;
                        $scope.waitAdjunto2 = false;
                        $scope.response = data;
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Se cargó el archivo',
                            showConfirmButton: false,
                            timer: 800
                        });
                        $scope.libro[id] = data.file_name;
                    }, function(error) {
                        $scope.waitAdjunto1 = false;
                        $scope.waitAdjunto2 = false;
                        Swal.fire(
                            'Error al cargar el archivo',
                            error.error,
                            'error'
                        );
                    });
                }
                fileReader.readAsArrayBuffer(file);
            };
            $scope.loadDepartamentos = function() {
                $scope.waitDepartamentos = true;
                $scope.waitProvincias = true;
                $scope.waitDistritos = true;
                return dataFactory.get('ubigeo/get_departamentos').then(function(data, status, headers, config) {
                    $scope.departamentos = data;
                    $scope.waitDepartamentos = false;
                    $scope.waitProvincias = false;
                    $scope.waitDistritos = false;
                });
            };
            $scope.loadProvincias = function() {
                $scope.waitProvincias = true;
                $scope.waitDistritos = true;
                return dataFactory.get('ubigeo/get_provincias/' + $scope.libro.departamento).then(function(data, status, headers, config) {
                    $scope.provincias = data;
                    $scope.waitProvincias = false;
                    $scope.waitDistritos = false;
                });
            };
            $scope.loadDistritos = function() {
                $scope.waitDistritos = true;
                return dataFactory.get('ubigeo/get_distritos/' + $scope.libro.departamento + '/' + $scope.libro.provincia).then(function(data, status, headers, config) {
                    $scope.distritos = data;
                    $scope.waitDistritos = false;
                });
            };
            $scope.toggleAdjunto2 = function() {
                $scope.showAdjunto2 = !$scope.showAdjunto2;
            };
            $scope.reset = function() {
                $scope.libro = {};
                $scope.libro.fecha_reclamo = new Date();
                $scope.libro.departamento = 'Lima';
                $scope.libro.provincia = 'Lima';
                $scope.libro.distrito = 'Lima';
                $scope.loadDepartamentos();
                $scope.loadProvincias();
                $scope.loadDistritos();
            };
            $scope.reset();
        })
        .controller("newsCtrl", function($scope, $sce, dataFactory) {
            $scope.category = 'noticias';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.noticias = data;
                $scope.noticias.value = $sce.trustAsHtml($scope.noticias.value);
            });
        })
        .controller("mainCarouselCtrl", function($scope, $sce, dataFactory) {
            $scope.carousels = [{
                    title: 'Inicial',
                    content: 'Nivel Inicial',
                    link: '<?php echo base_url(); ?>home/inicial',
                    img: '<?php echo base_url(); ?>assets/img/finicial.jpg'
                },
                {
                    title: 'Primaria',
                    content: 'Nivel Primaria',
                    link: '<?php echo base_url(); ?>home/primaria',
                    img: '<?php echo base_url(); ?>assets/img/fprimaria.jpg'
                },
                {
                    title: 'Secundaria',
                    content: 'Nivel Secundaria',
                    link: '<?php echo base_url(); ?>home/secundaria',
                    img: '<?php echo base_url(); ?>assets/img/fsecundaria.jpg'
                }
            ];
        })
        .controller("socialCtrl", function($scope, $sce, dataFactory) {
            $scope.category = 'facebook_link';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.facebook_link = data.value;
            });
            $scope.category = 'twitter_link';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.twitter_link = data.value;
            });
            $scope.category = 'youtube_link';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.youtube_link = data.value;
            });
        });
</script>