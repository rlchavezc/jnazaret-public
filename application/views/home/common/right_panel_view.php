<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
?>
<div class="col-md-2">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<div class="panel-title">
				<h4>Búsqueda</h4>
			</div>
		</div>
		<div class="panel-body">
			<form class="form-inline">
				<div class="input-group">
					<input type="text" class="form-control input-sm" id="search" required="required" placeholder="Buscar">
					<span class="input-group-btn">
						<button id="btn_search" type="submit" class="btn btn-sm btn-primary">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
				<a id="link_search" target="_blank" ></a>
			</form>
		</div>
	</div>
	<?php if (isset($output)): ?>
	<div class="panel panel-primary">
		<div class="panel-heading">
			<div class="panel-title">
				<h4>Galería</h4>
			</div>
		</div>
		<div class="panel-body">
			<?php foreach($css_files as $file): ?>
				<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
			<?php endforeach; ?>
			<?php foreach($js_files as $file): ?>
				<script src="<?php echo $file; ?>"></script>
			<?php endforeach; ?>
			<?php echo $output;?>
		</div>
	</div>
	<?php endif; ?>
	<script type="text/javascript">
		$("#btn_search").click(
			function(){
				var win = window.open("https://www.google.com.pe/search?hl=es&#hl=es&q="+$("#search").val()+"+site:jnazareth.edu.pe", "_blank");
			}
		);
	</script>
</div>