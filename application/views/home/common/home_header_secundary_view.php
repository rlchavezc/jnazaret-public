<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="row-fluid">
    <div class="col-lg-4 bg-custom-2 rounded">
        <h3>Nivel Inicial</h3>
        <a href="<?php echo base_url(); ?>home/inicial">
            <img alt="Nivel Inicial" src="<?php echo base_url(); ?>assets/img/finicial.jpg" class="img-responsive img-thumbnail" />
        </a>
    </div>
    <div class="col-lg-4 bg-custom-2 rounded">
        <h3>Nivel Primaria</h3>
        <a href="<?php echo base_url(); ?>home/primaria">
            <img alt="Nivel Inicial" src="<?php echo base_url(); ?>assets/img/fprimaria.jpg" class="img-responsive img-thumbnail" />
        </a>
    </div>
    <div class="col-lg-4 bg-custom-2 rounded">
        <h3>Nivel Secundaria</h3>
        <a href="<?php echo base_url(); ?>home/secundaria">
            <img alt="Nivel Inicial" src="<?php echo base_url(); ?>assets/img/fsecundaria.jpg" class="img-responsive img-thumbnail" />
        </a>
    </div>
</div>
<div class="clearfix"></div>
<hr />