<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="masthead">
    <div class="col-sm-2">
        <a href="<?php echo base_url(); ?>home/index">
            <img alt="Nivel Inicial" class="img-responsive" src="<?php echo base_url(); ?>assets/img/escudo.png" />
        </a>
    </div>
    <div class="col-sm-10">
        <h2 id="home-header-title">
            Colegio Privado <br /> Jesús de Nazareth
        </h2>
        <h2 id="home-header-subtitle"><em class="text-muted">"Caminando hacia la excelencia"</em></h2>
    </div>
    <div class="clearfix"></div>
    <nav>
        <ul class="nav nav-justified">
            <li><a href="<?php echo base_url(); ?>home/index">Inicio</a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    Quiénes somos <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo base_url(); ?>home/bienvenida">Bienvenida</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="<?php echo base_url(); ?>home/quienes_somos/collapseMision">Misión</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="<?php echo base_url(); ?>home/quienes_somos/collapseVision">Visión</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="<?php echo base_url(); ?>home/quienes_somos/collapseHistoria">Historia</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="<?php echo base_url(); ?>home/quienes_somos/collapsePerfil">Perfil</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="<?php echo base_url(); ?>home/quienes_somos/collapsePropuesta">Propuesta Educativa</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="<?php echo base_url(); ?>home/quienes_somos/collapseColaborador">Colaboradores</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="<?php echo base_url(); ?>home/admision" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    Admisión <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo base_url(); ?>home/admision">¿Por qué estudiar aquí?</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="<?php echo base_url(); ?>home/admision">Requisitos de Admisión.</a></li>
                </ul>
            </li>
            <li class="hidden"><a href="<?php echo base_url(); ?>home/actividades">Actividades</a></li>
            <li><a href="<?php echo base_url(); ?>home/galeria">Galería</a></li>
            <li><a href="<?php echo base_url(); ?>home/contactenos">Contáctenos</a></li>
            <li><a href="<?php echo base_url(); ?>home/exalumnos">Ex alumnos</a></li>
            <li><a href="<?php echo base_url(); ?>home/libro_reclamaciones">Libro de reclamaciones</a></li>
        </ul>
    </nav>
</div>
<style>
    .dropdown-menu {
        background-color: #223C95;
        /* 		background-color: #2A3764; */
        /* 		background-color: #E87918; */
    }

    .dropdown-menu>li>a {
        color: #fff;
        font-weight: bold;
    }
</style>