<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
?>
<div class="<?php echo isset($size)?$size:'col-md-4'; ?>" ng-controller="newsCtrl" id="noticias-panel">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<div class="panel-title">
				<h3>{{noticias.title}}</h3>
			</div>
		</div>
		<div class="panel-body" ng-bind-html="noticias.value">
			<!-- 
			<img id="pop-inicial" data-toggle='modal' data-target='#inicialModal' class="img-responsive" src="<?=base_url()?>assets/img/volantesI.jpg" alt="Inicial" />
			<img id="pop-secun" data-toggle='modal' data-target='#secundariaModal' class="img-responsive" src="<?=base_url()?>assets/img/volantesS.jpg" alt="Inicial" />
			-->
		</div>
	</div>
	<div class="modal fade" id="inicialModal" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4>Matrícula Inicial</h4>
				</div>
				<div class="modal-body">
					<img class="img-responsive" src="<?=base_url()?>assets/img/volantesI.jpg" alt="Inicial" />
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="secundariaModal" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4>Matrícula Secundaria</h4>
				</div>
				<div class="modal-body">
					<img class="img-responsive" src="<?=base_url()?>assets/img/volantesS.jpg" alt="Inicial" />
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>