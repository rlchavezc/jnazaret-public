<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<address id="address-footer" class="col-md-6">
    <!-- Jr. Crespo y Castillo 2648, Mirones Bajo, Lima | Teléfono: 4520248 - 7198213 | 2010  -->
    <strong style="font-family:Constantia;">Colegio Privado Jesús de Nazareth</strong><br>
    <a href="https://www.google.com.pe/maps/place/12%C2%B002'12.9%22S+77%C2%B004'29.0%22W/@-12.0372385,-77.0762274,17z/data=!4m2!3m1!1s0x0:0x0" rel="noreferrer" target="_blank">Jr. Crespo y Castillo 2648</a><br>
    Mirones Bajo, Lima<br>
    <abbr title="Teléfonos">Tel.:</abbr> 7198213
</address>