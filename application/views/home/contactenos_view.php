<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
?>
<div class="container">
	<?php $this->load->view('home/common/home_header_view');?>
	<?php $this->load->view('common/slider_common_view', array('id'=>'main_carousel', 'ng_controller'=>'mainCarouselCtrl'));?>
	<!-- Example row of columns -->
	<div class="row">
		<?php $this->load->view('home/common/left_panel_view');?>
		<div class="col-md-8">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="panel-title">
						<h3>Contáctenos</h3>
					</div>
				</div>
				<div class="panel-body" ng-controller="contactCtrl">
					<form class="form-horizontal">
						<div class="form-group">
							<label for="nombre" class="col-sm-2 control-label">Nombre</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="nombre" placeholder="Nombre" ng-model="contact.name" required="required"/>
							</div>
						</div>
						<div class="form-group">
							<label for="telefono" class="col-sm-2 control-label">Teléfono</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="telefono" placeholder="Teléfono" ng-model="contact.phone" required="required"/>
							</div>
						</div>
						<div class="form-group">
							<label for="correo" class="col-sm-2 control-label">Correo</label>
							<div class="col-sm-10">
								<input type="email" class="form-control" id="correo" placeholder="Correo" ng-model="contact.email" required="required"/>
							</div>
						</div>
						<div class="form-group">
							<label for="comentarios" class="col-sm-2 control-label">Comentarios</label>
							<div class="col-sm-10">
								<textarea class="form-control" rows="3" id="comentarios" placeholder="Comentarios" ng-model="contact.comment"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<div class="btn-group">
									<button type="submit" class="btn btn-primary" ng-click="mail()">Enviar</button>
									<button class="btn btn-default" ng-click="reset()">Borrar</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!--<?php $this->load->view('home/common/right_panel_view');?>-->
	</div>
</div>
<!-- /container -->
<script type="text/javascript">
	angular
	.module("mainApp", [])
	.factory("dataFactory", ['$http', '$q', '$location', 
		function($http, $q, $location){
			var dataBaseUrl = "<?php echo base_url();?>";
			var objFactory = {};
			objFactory.get = function(q) {
				return $http.get(dataBaseUrl + q).then(function(data, status, headers, config) {
					return data.data;
				}, function(data, status, headers, config) {
					console.log('[GET] ocurrió un error en ' + dataBaseUrl + q, data);
				});
			};
			objFactory.post = function(q, object) {
				return $http.post(dataBaseUrl + q, object).then(function(data, status, headers, config) {
					return data.data;
				}, function(data, status, headers, config) {
					console.log('[POST] ocurrió un error en ' + dataBaseUrl + q, data);
					console.log(object);
				});
			};
			return objFactory;
		}
	])
	.controller("contactCtrl", function ($scope, $sce, dataFactory) {
		$scope.mail = function(){
			if(!$scope.contact.email){
				alert("Advertencia!!.\n\nIngrese un correo válido");
				return false;
			}
			dataFactory.post('home/contact_mail', $scope.contact).then(function(data, status, headers, config) {
				$scope.response = data;
				$scope.reset();
			});
		};
		$scope.reset = function(){
			$scope.contact = {};
		};
	})
	.controller("newsCtrl", function ($scope, $sce, dataFactory) {
		$scope.category = 'noticias';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.noticias = data;
			$scope.noticias.value = $sce.trustAsHtml($scope.noticias.value);
		});
	})
	.controller("mainCarouselCtrl", function ($scope, $sce, dataFactory) {
		$scope.carousels = [
			{
				title: 'Inicial',
				content: 'Nivel Inicial',
				link: '<?php echo base_url();?>home/inicial',
				img: '<?php echo base_url();?>assets/img/finicial.jpg'
			},
			{
				title: 'Primaria',
				content: 'Nivel Primaria',
				link: '<?php echo base_url();?>home/primaria',
				img: '<?php echo base_url();?>assets/img/fprimaria.jpg'
			},
			{
				title: 'Secundaria',
				content: 'Nivel Secundaria',
				link: '<?php echo base_url();?>home/secundaria',
				img: '<?php echo base_url();?>assets/img/fsecundaria.jpg'
			}
		];
	})
	.controller("socialCtrl", function ($scope, $sce, dataFactory) {
		$scope.category = 'facebook_link';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.facebook_link = data.value;
		});
		$scope.category = 'twitter_link';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.twitter_link = data.value;
		});
		$scope.category = 'youtube_link';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.youtube_link = data.value;
		});
	});
</script>