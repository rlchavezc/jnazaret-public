<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
?>
<div class="container">
	<?php $this->load->view('home/common/home_header_view');?>
	<?php $this->load->view('common/slider_common_view', array('id'=>'main_carousel', 'ng_controller'=>'mainCarouselCtrl'));?>
	<!-- Example row of columns -->
	<div class="row">
		<?php $this->load->view('home/common/left_panel_view');?>
		<div class="col-md-8">
			<div class="panel panel-primary" ng-controller="contentCtrl">
				<div class="panel-heading">
					<div class="panel-title">
						<h3>{{content.title}}</h3>
					</div>
				</div>
				<div class="panel-body" ng-bind-html="content.value"></div>
				<?php $this->load->view('home/quienes_somos_accordion_view');?>
			</div>
		</div>
		<!--<?php $this->load->view('home/common/right_panel_view');?>-->
	</div>
</div>
<!-- /container -->
<script type="text/javascript">
	$("#<?=$section?>").collapse('show');
	angular
	.module("mainApp", [])
	.factory("dataFactory", ['$http', '$q', '$location', 
		function($http, $q, $location){
			var dataBaseUrl = "<?php echo base_url();?>";
			var objFactory = {};
			objFactory.get = function(q) {
				return $http.get(dataBaseUrl + q).then(function(data, status, headers, config) {
					return data.data;
				}, function(data, status, headers, config) {
					console.log('[GET] ocurrió un error en ' + dataBaseUrl + q, data);
				});
			};
			objFactory.post = function(q, object) {
				return $http.post(dataBaseUrl + q, object).then(function(data, status, headers, config) {
					return data.data;
				}, function(data, status, headers, config) {
					console.log('[POST] ocurrió un error en ' + dataBaseUrl + q, data);
					console.log(object);
				});
			};
			return objFactory;
		}
	])
	.controller("contentCtrl", function ($scope, $sce, dataFactory) {
		$scope.category = 'about';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.content = data;
			$scope.content.value = $sce.trustAsHtml($scope.content.value);
		});
		$scope.category = 'mision';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.mision_content = data;
			$scope.mision_content.value = $sce.trustAsHtml($scope.mision_content.value);
		});
		$scope.category = 'vision';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.vision_content = data;
			$scope.vision_content.value = $sce.trustAsHtml($scope.vision_content.value);
		});
		$scope.category = 'historia';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.historia_content = data;
			$scope.historia_content.value = $sce.trustAsHtml($scope.historia_content.value);
		});
		$scope.category = 'perfil';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.perfil_content = data;
			$scope.perfil_content.value = $sce.trustAsHtml($scope.perfil_content.value);
		});
		$scope.category = 'propuesta';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.propuesta_content = data;
			$scope.propuesta_content.value = $sce.trustAsHtml($scope.propuesta_content.value);
		});
		$scope.category = 'area_educativa';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.area_educativa_content = data;
			$scope.area_educativa_content.value = $sce.trustAsHtml($scope.area_educativa_content.value);
		});
		$scope.category = 'area_administrativa';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.area_administrativa_content = data;
			$scope.area_administrativa_content.value = $sce.trustAsHtml($scope.area_administrativa_content.value);
		});
	})
	.controller("newsCtrl", function ($scope, $sce, dataFactory) {
		$scope.category = 'noticias';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.noticias = data;
			$scope.noticias.value = $sce.trustAsHtml($scope.noticias.value);
		});
	})
	.controller("mainCarouselCtrl", function ($scope, $sce, dataFactory) {
		$scope.carousels = [
			{
				title: 'Inicial',
				content: 'Nivel Inicial',
				link: '<?php echo base_url();?>home/inicial',
				img: '<?php echo base_url();?>assets/img/finicial.jpg'
			},
			{
				title: 'Primaria',
				content: 'Nivel Primaria',
				link: '<?php echo base_url();?>home/primaria',
				img: '<?php echo base_url();?>assets/img/fprimaria.jpg'
			},
			{
				title: 'Secundaria',
				content: 'Nivel Secundaria',
				link: '<?php echo base_url();?>home/secundaria',
				img: '<?php echo base_url();?>assets/img/fsecundaria.jpg'
			}
		];
	})
	.controller("socialCtrl", function ($scope, $sce, dataFactory) {
		$scope.category = 'facebook_link';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.facebook_link = data.value;
		});
		$scope.category = 'twitter_link';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.twitter_link = data.value;
		});
		$scope.category = 'youtube_link';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.youtube_link = data.value;
		});
	});
</script>