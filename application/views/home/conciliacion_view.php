<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="container">
    <?php $this->load->view('home/common/home_header_view'); ?>
    <?php $this->load->view('common/slider_common_view', array('id' => 'main_carousel', 'ng_controller' => 'mainCarouselCtrl')); ?>
    <!-- Example row of columns -->
    <div class="row">
        <?php $this->load->view('home/common/left_panel_view'); ?>
        <div class="col-md-8">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h3>Conciliación</h3>
                    </div>
                </div>
                <div class="panel-body" ng-controller="conciliacionCtrl">
                    <h4>Confirmación de conciliación</h4>
                    <p>Sube tu documento</p>
                    <form class="form-horizontal" name="conciliacion_form">
                        <div class="form-group">
                            <label for="nombre" class="col-sm-2 control-label">Carga una foto</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <input type="file" class="form-control" id="foto1" placeholder="Foto1" required="required" />
                                    <input type="hidden" name="foto1" ng-model="conciliacion.foto1" required="required" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" ng-disabled="waitFoto1" ng-click="addFile('foto1')">Subir archivo</button>
                                    </span>
                                </div>
                                <span class="badge" ng-show="conciliacion_form.foto1.$invalid && !conciliacion_form.foto1.$pristine">La foto es obligatoria</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nombre" class="col-sm-2 control-label">Carga una foto</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <input type="file" class="form-control" id="foto2" placeholder="Foto2" required="required" />
                                    <input type="hidden" name="foto2" ng-model="conciliacion.foto2" required="required" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" ng-disabled="waitFoto2" ng-click="addFile('foto2')">Subir archivo</button>
                                    </span>
                                </div>
                                <span class="badge" ng-show="conciliacion_form.foto2.$invalid && !conciliacion_form.foto2.$pristine">La foto es obligatoria</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nombre" class="col-sm-2 control-label">Carga tu firma</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <input type="file" class="form-control" id="firma" placeholder="Firma" required="required" />
                                    <input type="hidden" name="firma" ng-model="conciliacion.firma" required="required" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" ng-disabled="waitFirma" ng-click="addFile('firma')">Subir archivo</button>
                                    </span>
                                </div>
                                <span class="badge" ng-show="conciliacion_form.firma.$invalid && !conciliacion_form.firma.$pristine">La foto es obligatoria</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nombre" class="col-sm-2 control-label">Código de hoja de reclamación</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <input type="number" class="form-control" id="id_reclamo" name="id_reclamo" placeholder="Código de hoja de reclamación" ng-model="conciliacion.id_reclamo" required="required" />
                                    <input type="hidden" name="validado" ng-model="conciliacion.validado" required="required" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" ng-disabled="waitReclamo" ng-click="validarIdReclamo()">Validar id reclamo</button>
                                    </span>
                                </div>
                                <span class="badge" ng-show="conciliacion_form.id_reclamo.$invalid && !conciliacion_form.id_reclamo.$pristine">El código de hoja de reclamación es obligatoria</span>
                                <span class="badge" ng-show="!conciliacion.validado && !conciliacion_form.id_reclamo.$pristine">El código de hoja de reclamación debe ser validado, presione "Validar id reclamo"</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="correo" class="col-sm-2 control-label">Correo</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="correo" name="correo" placeholder="Correo" ng-model="conciliacion.correo" required="required" />
                                <span class="badge" ng-show="conciliacion_form.correo.$invalid && !conciliacion_form.correo.$pristine">El correo es obligatorio</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fecha_conciliacion" class="col-sm-2 control-label">Fecha de conciliación</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control" id="fecha_conciliacion" name="fecha_conciliacion" placeholder="Fecha de conciliacion" ng-model="conciliacion.fecha_conciliacion" required="required" />
                                <span class="badge" ng-show="conciliacion_form.fecha_conciliacion.$invalid && !conciliacion_form.fecha_conciliacion.$pristine">La fecha de conciliacion es obligatoria</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="acciones" class="col-sm-2 control-label">Acciones tomadas por el colegio</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="3" id="acciones" name="acciones" placeholder="Acciones tomadas por el colegio" ng-model="conciliacion.acciones" required="required"></textarea>
                                <span class="badge" ng-show="conciliacion_form.acciones.$invalid && !conciliacion_form.acciones.$pristine">La lista de acciones es obligatoria</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-11">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="aceptacion_solucion" ng-model="conciliacion.aceptacion_solucion" required>* Envío mis documentos y acepto la solución acordada en la conciliación y las acciones por parte del negocio/empresa y finalizar mi reclamo/queja.</label>
                                    <span class="badge" ng-show="conciliacion_form.aceptacion_solucion.$invalid && !conciliacion_form.aceptacion_solucion.$pristine">Debe aceptar los términos</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-11">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="aceptacion_declaracion" ng-model="conciliacion.aceptacion_declaracion" required>* Declaro que soy el dueño del servicio y acepto el contenido de este formulario y que bajo Declaración Jurada la solución descrita es veraz.</label>
                                    <span class="badge" ng-show="conciliacion_form.aceptacion_declaracion.$invalid && !conciliacion_form.aceptacion_declaracion.$pristine">Debe aceptar los términos</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="btn-group">
                                    <ul class="hidden">
                                        <li ng-repeat="(key, errors) in conciliacion_form.$error track by $index"> <strong>{{ key }}</strong> errors
                                            <ul>
                                                <li ng-repeat="e in errors">{{ e.$name }} has an error: <strong>{{ key }}</strong>.</li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <button type="submit" ng-disabled="!conciliacion_form.$valid || waitConciliacion" class="btn btn-primary" ng-click="sendConciliacion()">Confirmar envío de documentos</button>
                                    <button class="btn btn-default" ng-click="reset()">Reestablecer formulario</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--<?php $this->load->view('home/common/right_panel_view'); ?>-->
    </div>
</div>
<!-- /container -->
<script type="text/javascript">
    angular
        .module("mainApp", [])
        .factory("dataFactory", ['$http', '$q', '$location',
            function($http, $q, $location) {
                var dataBaseUrl = "<?php echo base_url(); ?>";
                var objFactory = {};
                objFactory.get = function(q) {
                    return $http.get(dataBaseUrl + q).then(function(data, status, headers, config) {
                        return data.data;
                    }, function(data, status, headers, config) {
                        console.log('[GET] ocurrió un error en ' + dataBaseUrl + q, data);
                    });
                };
                objFactory.post = function(q, object) {
                    return $http.post(dataBaseUrl + q, object).then(function(data, status, headers, config) {
                        return data.data;
                    }, function(data, status, headers, config) {
                        console.log('[POST] ocurrió un error en ' + dataBaseUrl + q, data);
                        console.log(object);
                    });
                };
                objFactory.postFile = function(file, q, type) {
                    var fd = new FormData();
                    fd.append(type, file);
                    return $http.post(dataBaseUrl + q, fd, {
                            transformRequest: angular.identity,
                            headers: {
                                'Content-Type': undefined,
                                'Process-Data': false
                            }
                        })
                        .then(function(data) {
                            return data.data;
                        }, function() {
                            console.log('[POST] ocurrió un error en ' + uploadUrl, file);
                            console.log(object);
                        });
                };
                return objFactory;
            }
        ])
        .controller("conciliacionCtrl", function($scope, $sce, dataFactory) {
            $scope.waitFoto1 = false;
            $scope.waitFoto2 = false;
            $scope.waitFirma = false;
            $scope.waitReclamo = false;
            $scope.waitConciliacion = false;
            $scope.sendConciliacion = function() {
                $scope.waitConciliacion = true;
                $scope.conciliacion.fecha_conciliacion = moment($scope.conciliacion.fecha_conciliacion).format("YYYY-MM-DD HH:mm:ss");
                dataFactory.post('home/add_conciliacion', $scope.conciliacion).then(function(data, status, headers, config) {
                    $scope.waitConciliacion = false;
                    $scope.response = data;
                    Swal.fire(
                        'Registro satisfactorio',
                        `Su identificador de conciliación es: ${data.response}`,
                        'success'
                    );
                    $scope.reset();
                }, function(error) {
                    $scope.waitConciliacion = false;
                    Swal.fire(
                        'Error al registrar la conciliación',
                        error.error,
                        'error'
                    );
                });
            };
            $scope.addFile = function(id) {
                let file = document.getElementById(id).files[0],
                    fileReader = new FileReader();
                $scope.waitFoto1 = true;
                $scope.waitFoto2 = true;
                $scope.waitFirma = true;

                fileReader.onloadend = function(e) {
                    var src = e.target.result;
                    dataFactory.postFile(file, `upload/do_upload_conciliacion/${id}`, id).then(function(data, status, headers, config) {
                        $scope.waitFoto1 = false;
                        $scope.waitFoto2 = false;
                        $scope.waitFirma = false;
                        $scope.response = data;
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Se cargó el archivo',
                            showConfirmButton: false,
                            timer: 800
                        });
                        $scope.conciliacion[id] = data.file_name;
                    }, function(error) {
                        $scope.waitFoto1 = false;
                        $scope.waitFoto2 = false;
                        $scope.waitFirma = false;
                        Swal.fire(
                            'Error al cargar el archivo',
                            error.error,
                            'error'
                        );
                    });
                }
                fileReader.readAsArrayBuffer(file);
            }
            $scope.validarIdReclamo = function() {
                $scope.waitReclamo = true;
                let idReclamo = $scope.conciliacion.id_reclamo;
                dataFactory.get('home/data_reclamo_registrado/' + idReclamo).then(function(data, status, headers, config) {
                    $scope.waitReclamo = false;
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'El número de reclamo existe',
                        showConfirmButton: false,
                        timer: 800
                    });
                    $scope.conciliacion.validado = true;
                }, function(error) {
                    $scope.waitReclamo = false;
                    Swal.fire(
                        'Error',
                        error.error,
                        'error'
                    );
                    $scope.conciliacion.validado = false;
                });
            };
            $scope.reset = function() {
                $scope.conciliacion = {};
                $scope.conciliacion.fecha_conciliacion = new Date();
            };
            $scope.reset();
        })
        .controller("newsCtrl", function($scope, $sce, dataFactory) {
            $scope.category = 'noticias';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.noticias = data;
                $scope.noticias.value = $sce.trustAsHtml($scope.noticias.value);
            });
        })
        .controller("mainCarouselCtrl", function($scope, $sce, dataFactory) {
            $scope.carousels = [{
                    title: 'Inicial',
                    content: 'Nivel Inicial',
                    link: '<?php echo base_url(); ?>home/inicial',
                    img: '<?php echo base_url(); ?>assets/img/finicial.jpg'
                },
                {
                    title: 'Primaria',
                    content: 'Nivel Primaria',
                    link: '<?php echo base_url(); ?>home/primaria',
                    img: '<?php echo base_url(); ?>assets/img/fprimaria.jpg'
                },
                {
                    title: 'Secundaria',
                    content: 'Nivel Secundaria',
                    link: '<?php echo base_url(); ?>home/secundaria',
                    img: '<?php echo base_url(); ?>assets/img/fsecundaria.jpg'
                }
            ];
        })
        .controller("socialCtrl", function($scope, $sce, dataFactory) {
            $scope.category = 'facebook_link';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.facebook_link = data.value;
            });
            $scope.category = 'twitter_link';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.twitter_link = data.value;
            });
            $scope.category = 'youtube_link';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.youtube_link = data.value;
            });
        });
</script>