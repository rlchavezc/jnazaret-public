<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
?>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	<div class="panel panel-primary">
		<div class="panel-heading" role="tab" id="headingEstudiar">
			<h4 class="panel-title">
				<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEstudiar" aria-expanded="true" aria-controls="collapseEstudiar">
					{{estudiar_content.title}}
				</a>
			</h4>
		</div>
		<div id="collapseEstudiar" class="panel-collapse collapse"
			role="tabpanel" aria-labelledby="headingEstudiar">
			<div class="panel-body" ng-bind-html="estudiar_content.value">
			</div>
		</div>
	</div>
	<div class="panel panel-primary">
		<div class="panel-heading" role="tab" id="headingRequisitos">
			<h4 class="panel-title">
				<a class="collapsed" role="button" data-toggle="collapse"
					data-parent="#accordion" href="#collapseRequisitos"
					aria-expanded="false" aria-controls="collapseRequisitos">{{requisitos_content.title}}</a>
			</h4>
		</div>
		<div id="collapseRequisitos" class="panel-collapse collapse"
			role="tabpanel" aria-labelledby="headingRequisitos">
			<div class="panel-body" ng-bind-html="requisitos_content.value">
			</div>
		</div>
	</div>
</div>