<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
?>
<div class="container">
	<?php $this->load->view('home/common/home_header_view');?>
	<?php $this->load->view('common/slider_common_view', array('id'=>'main_carousel', 'ng_controller'=>'mainCarouselCtrl'));?>
	<!-- Example row of columns -->
	<div class="row">
		<?php $this->load->view('home/common/left_panel_view');?>
		<div class="col-md-8">
			<div class="panel panel-primary" ng-controller="contentCtrl">
				<div class="panel-heading">
					<div class="panel-title">
						<h3>{{content.title}}</h3>
					</div>
				</div>
				<div class="panel-body" ng-bind-html="content.value"></div>
			</div>
		</div>
		<!--<?php $this->load->view('home/common/right_panel_view');?>-->
	</div>
</div>
<!-- /container -->
<script type="text/javascript">
	angular
	.module("mainApp", [])
	.factory("dataFactory", ['$http', '$q', '$location', 
		function($http, $q, $location){
			var dataBaseUrl = "<?php echo base_url();?>";
			var objFactory = {};
			objFactory.get = function(q) {
				return $http.get(dataBaseUrl + q).then(function(data, status, headers, config) {
					return data.data;
				}, function(data, status, headers, config) {
					console.log('[GET] ocurrió un error en ' + dataBaseUrl + q, data);
				});
			};
			objFactory.post = function(q, object) {
				return $http.post(dataBaseUrl + q, object).then(function(data, status, headers, config) {
					return data.data;
				}, function(data, status, headers, config) {
					console.log('[POST] ocurrió un error en ' + dataBaseUrl + q, data);
					console.log(object);
				});
			};
			return objFactory;
		}
	])
	.controller("contentCtrl", function ($scope, $sce, dataFactory) {
		$scope.category = 'welcome';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.content = data;
			$scope.content.value = $sce.trustAsHtml($scope.content.value);
		});
	})
	.controller("newsCtrl", function ($scope, $sce, dataFactory) {
		$scope.category = 'noticias';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.noticias = data;
			$scope.noticias.value = $sce.trustAsHtml($scope.noticias.value);
		});
	})
	.controller("mainCarouselCtrl", function ($scope, $sce, dataFactory) {
		$scope.carousels = [
			{
				title: 'Inicial',
				content: 'Nivel Inicial',
				link: '<?php echo base_url();?>home/inicial',
				img: '<?php echo base_url();?>assets/img/finicial.jpg'
			},
			{
				title: 'Primaria',
				content: 'Nivel Primaria',
				link: '<?php echo base_url();?>home/primaria',
				img: '<?php echo base_url();?>assets/img/fprimaria.jpg'
			},
			{
				title: 'Secundaria',
				content: 'Nivel Secundaria',
				link: '<?php echo base_url();?>home/secundaria',
				img: '<?php echo base_url();?>assets/img/fsecundaria.jpg'
			}
		];
	})
	.controller("socialCtrl", function ($scope, $sce, dataFactory) {
		$scope.category = 'facebook_link';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.facebook_link = data.value;
		});
		$scope.category = 'twitter_link';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.twitter_link = data.value;
		});
		$scope.category = 'youtube_link';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.youtube_link = data;
		});
	});
	$('#pop-inicial').click();
</script>