<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
?>
<div class="container">
	<?php $this->load->view('home/common/home_header_view');?>
	<?php $this->load->view('common/slider_common_view', array('id'=>'main_carousel', 'ng_controller'=>'mainCarouselCtrl'));?>
	<!-- Example row of columns -->
	<div class="row">
		<?php $this->load->view('home/common/left_panel_view');?>
		<div class="col-md-8">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="panel-title">
						<h3>Ex-Alumnos</h3>
					</div>
				</div>
				<div class="panel-body" ng-controller="exalumnoCtrl">
					<form class="form-horizontal">
						<p>Si eres Ex-Alumno nos interesa saber de ti, puedes registrarte para mantenernos en contacto con las actividades del colegio.</p>
						<div class="form-group">
							<label for="apellidos" class="col-sm-2 control-label">Apellidos</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="apellidos" placeholder="Apellidos" ng-model="exalumno.last_name" required="required"/>
							</div>
						</div>
						<div class="form-group">
							<label for="nombre" class="col-sm-2 control-label">Nombres</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="nombres" placeholder="Nombres" ng-model="exalumno.name" required="required"/>
							</div>
						</div>
						<div class="form-group">
							<label for="telefono" class="col-sm-2 control-label">Teléfono</label>
							<div class="col-sm-10">
								<input type="tel" class="form-control" id="telefono" placeholder="Teléfono" ng-model="exalumno.phone" required="required"/>
							</div>
						</div>
						<div class="form-group">
							<label for="correo" class="col-sm-2 control-label">Correo</label>
							<div class="col-sm-10">
								<input type="email" class="form-control" id="correo" placeholder="Correo" ng-model="exalumno.email" required="required"/>
							</div>
						</div>
						<div class="form-group">
							<label for="direccion" class="col-sm-2 control-label">Dirección</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="direccion" placeholder="Dirección" ng-model="exalumno.address" required="required"/>
							</div>
						</div>
						<div class="form-group">
							<label for="egreso" class="col-sm-2 control-label">Año de egreso</label>
							<div class="col-sm-10">
								<input type="number" class="form-control" id="egreso" placeholder="Año de egreso" ng-model="exalumno.year_finish" required="required"/>
							</div>
						</div>
						<div class="form-group">
							<label for="centro" class="col-sm-2 control-label"><abbr title="Centro">C.</abbr> Laboral y/o estudios</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="centro" placeholder="C. Laboral y/o estudios" ng-model="exalumno.name_place" required="required"/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<div class="btn-group">
									<button type="submit" class="btn btn-primary" ng-click="send()">Enviar</button>
									<button class="btn btn-default" ng-click="reset()">Borrar</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!--<?php $this->load->view('home/common/right_panel_view', $output);?>-->
	</div>
</div>
<!-- /container -->
<script type="text/javascript">
	angular
	.module("mainApp", [])
	.factory("dataFactory", ['$http', '$q', '$location', 
		function($http, $q, $location){
			var dataBaseUrl = "<?php echo base_url();?>";
			var objFactory = {};
			objFactory.get = function(q) {
				return $http.get(dataBaseUrl + q).then(function(data, status, headers, config) {
					return data.data;
				}, function(data, status, headers, config) {
					console.log('[GET] ocurrió un error en ' + dataBaseUrl + q, data);
				});
			};
			objFactory.post = function(q, object) {
				return $http.post(dataBaseUrl + q, object).then(function(data, status, headers, config) {
					return data.data;
				}, function(data, status, headers, config) {
					console.log('[POST] ocurrió un error en ' + dataBaseUrl + q, data);
					console.log(object);
				});
			};
			return objFactory;
		}
	])
	.controller("exalumnoCtrl", function ($scope, $sce, dataFactory) {
		$scope.send = function(){
			dataFactory.post('home/add_exalumno', $scope.exalumno).then(function(data, status, headers, config) {
				$scope.response = data;
				$scope.reset();
				alert('Se registró su información correctamente.');
			});
		};
		$scope.reset = function(){
			$scope.exalumno = {};
		};
	})
	.controller("newsCtrl", function ($scope, $sce, dataFactory) {
		$scope.category = 'noticias';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.noticias = data;
			$scope.noticias.value = $sce.trustAsHtml($scope.noticias.value);
		});
	})
	.controller("mainCarouselCtrl", function ($scope, $sce, dataFactory) {
		$scope.carousels = [
			{
				title: 'Inicial',
				content: 'Nivel Inicial',
				link: '<?php echo base_url();?>home/inicial',
				img: '<?php echo base_url();?>assets/img/finicial.jpg'
			},
			{
				title: 'Primaria',
				content: 'Nivel Primaria',
				link: '<?php echo base_url();?>home/primaria',
				img: '<?php echo base_url();?>assets/img/fprimaria.jpg'
			},
			{
				title: 'Secundaria',
				content: 'Nivel Secundaria',
				link: '<?php echo base_url();?>home/secundaria',
				img: '<?php echo base_url();?>assets/img/fsecundaria.jpg'
			}
		];
	})
	.controller("socialCtrl", function ($scope, $sce, dataFactory) {
		$scope.category = 'facebook_link';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.facebook_link = data.value;
		});
		$scope.category = 'twitter_link';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.twitter_link = data.value;
		});
		$scope.category = 'youtube_link';
		dataFactory.get('home/data_content/'+$scope.category).then(function(data, status, headers, config) {
			$scope.youtube_link = data.value;
		});
	});
</script>