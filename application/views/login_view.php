<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="container" style="padding-top: 100px; padding-bottom: 100px;">
    <div class="row">
        <div class="col-md-offset-4 col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        <?php echo $sys_name; ?>
                    </div>
                </div>
                <div class="panel-body" ng-controller="loginCtrl">
                    <div class="alert alert-success" id="login_response" style="display: none;">
                        <a class="close" onclick="$('#login_response').hide('fade out');">&times;</a>
                        <p>Resultado del login</p>
                    </div>
                    <form class="form-horizontal" name="login_form">
                        <div class="control-group">
                            <label for="user">Usuario</label>
                            <div class="controls">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                                    <input class="form-control" type="text" name="user" id="user" placeholder="Ingrese su usuario" ng-model="login.user" required="required" />
                                </div>
                                <span class="badge" ng-show="login_form.user.$invalid && !login_form.user.$pristine">El usuario es obligatorio</span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="user">Contraseña</label>
                            <div class="controls">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                                    <input class="form-control" type="password" name="pass" id="pass" placeholder="Ingrese su contraseña" ng-model="login.pass" required="required" />
                                </div>
                                <span class="badge" ng-show="login_form.pass.$invalid && !login_form.pass.$pristine">La contraseña es obligatoria</span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for=""></label>
                            <div class="controls">
                                <ul class="hidden">
                                    <li ng-repeat="(key, errors) in login_form.$error track by $index"> <strong>{{ key }}</strong> errors
                                        <ul>
                                            <li ng-repeat="e in errors">{{ e.$name }} has an error: <strong>{{ key }}</strong>.</li>
                                        </ul>
                                    </li>
                                </ul>
                                <div class="btn-group">
                                    <button class="btn btn-primary" ng-disabled="!login_form.$valid || waitLogin" ng-click="send()"><i class="fa fa-sign-in" aria-hidden="true"></i> Ingresar</button>
                                    <button class="btn btn-default" ng-click="reset()"><i class="fa fa-undo" aria-hidden="true"></i> Cancelar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    angular
        .module("mainApp", [])
        .factory("dataFactory", ['$http', '$q', '$location',
            function($http, $q, $location) {
                var dataBaseUrl = "<?php echo base_url(); ?>";
                var objFactory = {};
                objFactory.get = function(q) {
                    return $http.get(dataBaseUrl + q).then(function(data, status, headers, config) {
                        return data.data;
                    }, function(data, status, headers, config) {
                        console.log('[GET] ocurrió un error en ' + dataBaseUrl + q, data);
                    });
                };
                objFactory.post = function(q, object) {
                    return $http.post(dataBaseUrl + q, object).then(function(data, status, headers, config) {
                        return data.data;
                    }, function(data, status, headers, config) {
                        console.log('[POST] ocurrió un error en ' + dataBaseUrl + q, data);
                        console.log(object);
                        throw "error";
                    });
                };
                return objFactory;
            }
        ])
        .controller("loginCtrl", function($scope, $sce, dataFactory) {
            $scope.login = {};
            $scope.waitLogin = false;
            $scope.send = function() {
                $scope.waitLogin = true;
                dataFactory.post('login/do_login', $scope.login).then(function(data, status, headers, config) {
                    $scope.waitLogin = false;
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Login realizado correctamente',
                        showConfirmButton: false,
                        timer: 800
                    });
                    $scope.reset();
                    window.location.assign("<?php echo base_url(); ?>admin");
                }, function(error) {
                    $scope.waitLogin = false;
                    Swal.fire(
                        'Usuario y password incorrectos!',
                        error.error,
                        'error'
                    );
                    $scope.reset();
                });
            };
            $scope.reset = function() {
                $scope.login = {};
            };
            $scope.reset();
        })
        .controller("socialCtrl", function($scope, $sce, dataFactory) {
            $scope.category = 'facebook_link';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.facebook_link = data.value;
            });
            $scope.category = 'twitter_link';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.twitter_link = data.value;
            });
            $scope.category = 'youtube_link';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.youtube_link = data.value;
            });
        });
</script>