<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="container-fluid" ng-controller="libroreclamacionCtrl">
    <div class="row row-offcanvas row-offcanvas-left">
        <div class="col-sm-3 col-md-2 sidebar-offcanvas" role="navigation">
            <ul class="nav nav-sidebar col-sm-3 col-md-2 affix" style="height: 100%;" ng-controller="sidebarCtrl">
                <li ng-repeat="item in sidebar track by $index" ng-class="{'active': item.active==1}"><a ng-href="{{item.url}}">{{item.label}}</a>
                </li>
            </ul>
        </div>
        <div class="col-sm-9 col-md-10">
            <p class="visible-xs">
                <button type="button" class="btn btn-primary btn-sm off-canvas-toggle" data-toggle="offcanvas">
                    <i class="glyphicon glyphicon-chevron-left" aria-hidden="true"></i>
                </button>
            </p>
            <h1 class="page-header text-muted">Lista de libro de reclamaciones</h1>
            <div class="row placeholders">
                <div class="col-md-12 placeholder">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4>
                                    <i class="fa fa-list-ul pull-right" aria-hidden="true"></i> Resumen de libro de reclamaciones
                                </h4>
                            </div>
                        </div>
                        <div class="panel-body">Lista de libro de reclamaciones</div>
                        <p>
                            <span class="label label-default"> libro de reclamación </span>
                        </p>
                        <div class="controls">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
                                <input class="form-control" id="searchLibroReclamacion" placeholder="Filtrar libro de reclamación" ng-model="searchLibroReclamacion" type="text" /> <span class="input-group-addon" ng-click="searchLibroReclamacion = ''"><i class="fa fa-eraser" aria-hidden="true"></i>
                                    Borrar</span>
                            </div>
                        </div>
                        <hr />
                        <div class="table-responsive">
                            <table aria-hidden="true" class="table table-condensed table-hover">
                                <thead>
                                    <tr class="info">
                                        <th scope="row">
                                            <a href="" ng-click="sortLibroReclamacionType = 'id'; sortLibroReclamacionReverse = !sortLibroReclamacionReverse">
                                                # </a>
                                        </th>
                                        <th scope="row">
                                            <a href="" ng-click="sortLibroReclamacionType = 'nombres'; sortLibroReclamacionReverse = !sortLibroReclamacionReverse">
                                                Nombres
                                            </a>
                                        </th>
                                        <th scope="row">
                                            <a href="" ng-click="sortLibroReclamacionType = 'primer_apellido'; sortLibroReclamacionReverse = !sortLibroReclamacionReverse">
                                                Primer apellido
                                            </a>
                                        </th>
                                        <th scope="row">
                                            <a href="" ng-click="sortLibroReclamacionType = 'segundo_apellido'; sortLibroReclamacionReverse = !sortLibroReclamacionReverse">
                                                Segundo Apellido
                                            </a>
                                        </th>
                                        <th scope="row" ng-if="false">
                                            <a href="" ng-click="sortLibroReclamacionType = 'tipo_documento'; sortLibroReclamacionReverse = !sortLibroReclamacionReverse">
                                                Tipo de documento
                                            </a>
                                        </th>
                                        <th scope="row" ng-if="false">
                                            <a href="" ng-click="sortLibroReclamacionType = 'numero_documento'; sortLibroReclamacionReverse = !sortLibroReclamacionReverse">
                                                Numero de documento
                                            </a>
                                        </th>
                                        <th scope="row" ng-if="false">
                                            <a href="" ng-click="sortLibroReclamacionType = 'celular'; sortLibroReclamacionReverse = !sortLibroReclamacionReverse">
                                                Celular
                                            </a>
                                        </th>
                                        <th scope="row" ng-if="false">
                                            <a href="" ng-click="sortLibroReclamacionType = 'departamento'; sortLibroReclamacionReverse = !sortLibroReclamacionReverse">
                                                Departamento
                                            </a>
                                        </th>
                                        <th scope="row" ng-if="false">
                                            <a href="" ng-click="sortLibroReclamacionType = 'provincia'; sortLibroReclamacionReverse = !sortLibroReclamacionReverse">
                                                Provincia
                                            </a>
                                        </th>
                                        <th scope="row" ng-if="false">
                                            <a href="" ng-click="sortLibroReclamacionType = 'distrito'; sortLibroReclamacionReverse = !sortLibroReclamacionReverse">
                                                Distrito
                                            </a>
                                        </th>
                                        <th scope="row" ng-if="false">
                                            <a href="" ng-click="sortLibroReclamacionType = 'direccion'; sortLibroReclamacionReverse = !sortLibroReclamacionReverse">
                                                Dirección
                                            </a>
                                        </th>
                                        <th scope="row" ng-if="false">
                                            <a href="" ng-click="sortLibroReclamacionType = 'referencia'; sortLibroReclamacionReverse = !sortLibroReclamacionReverse">
                                                Referencia
                                            </a>
                                        </th>
                                        <th scope="row" ng-if="false">
                                            <a href="" ng-click="sortLibroReclamacionType = 'correo'; sortLibroReclamacionReverse = !sortLibroReclamacionReverse">
                                                Correo
                                            </a>
                                        </th>
                                        <th scope="row" ng-if="false">
                                            <a href="" ng-click="sortLibroReclamacionType = 'menor_edad'; sortLibroReclamacionReverse = !sortLibroReclamacionReverse">
                                                Menor de edad
                                            </a>
                                        </th>
                                        <th scope="row">
                                            <a href="" ng-click="sortLibroReclamacionType = 'tipo_reclamo'; sortLibroReclamacionReverse = !sortLibroReclamacionReverse">
                                                Tipo de reclamo
                                            </a>
                                        </th>
                                        <th scope="row">
                                            <a href="" ng-click="sortLibroReclamacionType = 'fecha_reclamo'; sortLibroReclamacionReverse = !sortLibroReclamacionReverse">
                                                Fecha de reclamo
                                            </a>
                                        </th>
                                        <th scope="row">
                                            <a href="" ng-click="sortLibroReclamacionType = 'conciliacion.fecha_conciliacion'; sortLibroReclamacionReverse = !sortLibroReclamacionReverse">
                                                Fecha de conciliación
                                            </a>
                                        </th>
                                        <th scope="row" ng-if="false">
                                            <a href="" ng-click="sortLibroReclamacionType = 'descripcion'; sortLibroReclamacionReverse = !sortLibroReclamacionReverse">
                                                Descripción
                                            </a>
                                        </th>
                                        <th scope="row">
                                            <a href="" ng-click="sortLibroReclamacionType = 'estado'; sortLibroReclamacionReverse = !sortLibroReclamacionReverse">
                                                Estado
                                            </a>
                                        </th>
                                        <th scope="row">
                                            Acciones
                                        </th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td colspan="9">
                                            <div class="table-responsive">
                                                <table aria-hidden="true" class="table table-condensed table-hover">
                                                    <thead>
                                                        <tr class="info">
                                                            <th scope="row" ng-repeat="item in orderChart">{{item.label}}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td ng-repeat="item in orderChart">{{item.value}}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <tr ng-repeat="item in libro_reclamaciones | orderBy:sortLibroReclamacionType:sortLibroReclamacionReverse | filter: searchLibroReclamacion" ng-class="item.class_name">
                                        <td>
                                            <button type="button" class="btn btn-info btn-xs" data-toggle='modal' data-target='#detailModal' title="Ver detalle de reclamación" ng-click="detail(item)">
                                                {{item.id}} <i class="fa fa-eye" aria-hidden="true"></i>
                                            </button>
                                        </td>
                                        <td>{{item.nombres}}</td>
                                        <td>{{item.primer_apellido}}</td>
                                        <td>{{item.segundo_apellido}}</td>
                                        <td ng-if="false">{{item.tipo_documento}}</td>
                                        <td ng-if="false">{{item.numero_documento}}</td>
                                        <td ng-if="false">{{item.celular}}</td>
                                        <td ng-if="false">{{item.departamento}}</td>
                                        <td ng-if="false">{{item.provincia}}</td>
                                        <td ng-if="false">{{item.distrito}}</td>
                                        <td ng-if="false">{{item.direccion}}</td>
                                        <td ng-if="false">{{item.referencia}}</td>
                                        <td ng-if="false"><a href="mailto:{{item.correo}}">{{item.correo}}</a></td>
                                        <td ng-if="false">{{item.menor_edad}}</td>
                                        <td>{{item.tipo_reclamo}}</td>
                                        <td>{{item.fecha_reclamo}}</td>
                                        <td>{{item.conciliacion.fecha_conciliacion}}</td>
                                        <td ng-if="false">{{item.descripcion}}</td>
                                        <td><span class="label label-info">{{item.estado}}</span></td>
                                        <td><button type="button" class="btn btn-danger btn-xs" data-toggle='modal' data-target='#confirmModal' title="Borrar Exalumno" ng-click="prev(item)"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2 alert" ng-class="popup.type" ng-show="popup.show" style="position: fixed; bottom: 0px; left: 0px;">
        <a class="close" ng-click="popup.show = false"><sup>&times;</sup></a>
        <label>{{popup.title}}</label>
        <p>{{popup.message}}</p>
    </div>
    <div class="modal fade" id="confirmModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="text-muted"><i class="fa fa-history" aria-hidden="true"></i> Eliminar registro </h4>
                </div>
                <div class="modal-body">
                    <p>¿Está seguro de eliminar el registro?</p>
                    <div class="btn-group">
                        <button class="btn btn-primary" data-dismiss="modal" ng-click="del(delete_exalumno.id)"><i class="fa fa-trash" aria-hidden="true"></i> Sí</button>
                        <button class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="detailModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="text-muted"><i class="fa fa-eye" aria-hidden="true"></i> Detalle de hoja de reclamación </h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-6">
                        <p>Detalle:</p>
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label for="nombres" class="col-sm-2 control-label">Nombres</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nombres" placeholder="Nombres" ng-model="detail_libro.nombres" ng-disabled="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="primer_apellido" class="col-sm-2 control-label">Primer Apellido</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="primer_apellido" placeholder="Primer apellido" ng-model="detail_libro.primer_apellido" ng-disabled="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="primer_apellido" class="col-sm-2 control-label">Segundo Apellido</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="segundo_apellido" placeholder="Segundo apellido" ng-model="detail_libro.segundo_apellido" ng-disabled="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tipo_documento" class="col-sm-2 control-label">Tipo de documento</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="tipo_documento" placeholder="Tipo de documento" ng-model="detail_libro.tipo_documento" ng-disabled="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="numero_documento" class="col-sm-2 control-label">Número de documento</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="numero_documento" placeholder="Número de documento" ng-model="detail_libro.numero_documento" ng-disabled="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="celular" class="col-sm-2 control-label">Celular</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="celular" placeholder="Celular" ng-model="detail_libro.celular" ng-disabled="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="departamento" class="col-sm-2 control-label">Departamento</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="departamento" placeholder="Departamento" ng-model="detail_libro.departamento" ng-disabled="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="provincia" class="col-sm-2 control-label">Provincia</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="provincia" placeholder="Provincia" ng-model="detail_libro.provincia" ng-disabled="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="distrito" class="col-sm-2 control-label">Distrito</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="distrito" placeholder="Distrito" ng-model="detail_libro.distrito" ng-disabled="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="direccion" class="col-sm-2 control-label">Dirección</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="direccion" placeholder="Dirección" ng-model="detail_libro.direccion" ng-disabled="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="referencia" class="col-sm-2 control-label">Referencia</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="referencia" placeholder="Referencia" ng-model="detail_libro.referencia" ng-disabled="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="correo" class="col-sm-2 control-label">Correo</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="correo" placeholder="Correo" ng-model="detail_libro.correo" ng-disabled="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="menor_edad" class="col-sm-2 control-label">Menor de edad</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="menor_edad" placeholder="Menor de edad" ng-model="detail_libro.menor_edad" ng-disabled="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tipo_reclamo" class="col-sm-2 control-label">Tipo de reclamo</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="tipo_reclamo" placeholder="Tipo de reclamo" ng-model="detail_libro.tipo_reclamo" ng-disabled="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="fecha_reclamo" class="col-sm-2 control-label">Fecha de reclamo</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="fecha_reclamo" placeholder="Fecha de reclamo" ng-model="detail_libro.fecha_reclamo" ng-disabled="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="descripcion" class="col-sm-2 control-label">Descripción</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="3" id="descripcion" name="descripcion" placeholder="Descripción" ng-model="detail_libro.descripcion" ng-disabled="true"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="foto1_url" class="col-sm-2 control-label">Adjunto 1</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="adjunto1_url" placeholder="No se subió Adjunto 1" ng-model="detail_libro.adjunto1_url" ng-disabled="true" />
                                    <a ng-href="{{detail_libro.adjunto1_url}}" target="_blank">Abrir archivo <i class="fa fa-eye" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="foto2_url" class="col-sm-2 control-label">Adjunto 2</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="adjunto2_url" placeholder="No se subió Adjunto 2" ng-model="detail_libro.adjunto2_url" ng-disabled="true" />
                                    <a ng-href="{{detail_libro.adjunto2_url}}" target="_blank">Abrir archivo <i class="fa fa-eye" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="estado" class="col-sm-2 control-label">Estado</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="estado" placeholder="Estado" ng-model="detail_libro.estado" ng-disabled="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="created_at" class="col-sm-2 control-label">Fecha y hora de registro</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="created_at" placeholder="Fecha y hora de registro" ng-model="detail_libro.created_at" ng-disabled="true" />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <p>Detalle de conciliación:</p>
                        <p ng-if="!detail_libro.conciliacion">No se ha registrado la conciliación del reclamo.</p>
                        <form class="form-horizontal" ng-if="detail_libro.conciliacion">
                            <div class="form-group">
                                <label for="id" class="col-sm-2 control-label">Id conciliación</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="conciliacion-id" placeholder="Id conciliación" ng-model="detail_libro.conciliacion.id" ng-disabled="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="foto1_url" class="col-sm-2 control-label">Adjunto 1</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="conciliacion-foto1_url" placeholder="No se subió Adjunto 1" ng-model="detail_libro.conciliacion.foto1_url" ng-disabled="true" />
                                    <a ng-href="{{detail_libro.conciliacion.foto1_url}}" target="_blank">Abrir archivo <i class="fa fa-eye" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="foto2_url" class="col-sm-2 control-label">Adjunto 2</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="conciliacion-foto2_url" placeholder="No se subió Adjunto 2" ng-model="detail_libro.conciliacion.foto2_url" ng-disabled="true" />
                                    <a ng-href="{{detail_libro.conciliacion.foto2_url}}" target="_blank">Abrir archivo <i class="fa fa-eye" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="firma_url" class="col-sm-2 control-label">Firma</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="conciliacion-firma_url" placeholder="No se subió Firma" ng-model="detail_libro.conciliacion.firma_url" ng-disabled="true" />
                                    <a ng-href="{{detail_libro.conciliacion.firma_url}}" target="_blank">Abrir archivo <i class="fa fa-eye" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="id_reclamo" class="col-sm-2 control-label">Id de reclamo</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="conciliacion-id_reclamo" placeholder="Id de reclamo" ng-model="detail_libro.conciliacion.id_reclamo" ng-disabled="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="correo" class="col-sm-2 control-label">Correo</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="conciliacion-correo" placeholder="Correo" ng-model="detail_libro.conciliacion.correo" ng-disabled="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="fecha_conciliacion" class="col-sm-2 control-label">Fecha de conciliación</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="conciliacion-fecha_conciliacion" placeholder="Fecha de conciliación" ng-model="detail_libro.conciliacion.fecha_conciliacion" ng-disabled="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="acciones" class="col-sm-2 control-label">Acciones</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="3" id="acciones" name="acciones" placeholder="Acciones" ng-model="detail_libro.conciliacion.acciones" ng-disabled="true"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="created_at" class="col-sm-2 control-label">Fecha de registro de conciliación</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="conciliacion-created_at" placeholder="Acciones" ng-model="detail_libro.conciliacion.created_at" ng-disabled="true" />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="btn-group">
                        <button class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $(".select-autocomplete").select2({
            placeholder: "Seleccione uno",
            allowClear: true
        });
        $('#editor').wysiwyg();
        $('#editor').cleanHtml();
    });
    $('.off-canvas-toggle').on('click', function(event) {
        event.preventDefault();
        $('.row-offcanvas-left').toggleClass('active');
    });
    angular
        .module("mainApp", [])
        .factory("dataFactory", ['$http', '$q', '$location',
            function($http, $q, $location) {
                var dataBaseUrl = "<?php echo base_url(); ?>";
                var objFactory = {};
                objFactory.get = function(q) {
                    return $http.get(dataBaseUrl + q).then(function(data, status, headers, config) {
                        return data.data;
                    }, function(data, status, headers, config) {
                        console.log('[GET] ocurrió un error en ' + dataBaseUrl + q, data);
                    });
                };
                objFactory.post = function(q, object) {
                    return $http.post(dataBaseUrl + q, object).then(function(data, status, headers, config) {
                        return data.data;
                    }, function(data, status, headers, config) {
                        console.log('[POST] ocurrió un error en ' + dataBaseUrl + q, data);
                        console.log(object);
                    });
                };
                return objFactory;
            }
        ])
        .controller("sidebarCtrl", function($scope, dataFactory) {
            dataFactory.get('admin/sidebar/5').then(function(data, status, headers, config) {
                $scope.sidebar = data;
            });
        })
        .controller("libroreclamacionCtrl", function($scope, $sce, dataFactory) {
            $scope.popup = {
                type: 'alert-info',
                show: false,
                title: 'Información',
                message: 'Descripción de la información'
            };
            $scope.load = function() {
                dataFactory.get('admin/data_libro_reclamaciones').then(function(data, status, headers, config) {
                    $scope.libro_reclamaciones = data;
                });
            };
            $scope.del = function(id) {
                dataFactory.get('admin/delete_exalumno/' + id).then(function(data, status, headers, config) {
                    $scope.load();
                })
            };
            $scope.prev = function(item) {
                $scope.delete_exalumno = item;
            };
            $scope.detail = function(item) {
                $scope.detail_libro = item;
            };
            $scope.load();
        })
        .controller("socialCtrl", function($scope, $sce, dataFactory) {
            $scope.category = 'facebook_link';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.facebook_link = data.value;
            });
            $scope.category = 'twitter_link';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.twitter_link = data.value;
            });
            $scope.category = 'youtube_link';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.youtube_link = data.value;
            });
        });
</script>