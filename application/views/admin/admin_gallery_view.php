<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="container-fluid" ng-controller="exalumnoCtrl">
    <div class="row row-offcanvas row-offcanvas-left">
        <div class="col-sm-3 col-md-2 sidebar-offcanvas" role="navigation">
            <ul class="nav nav-sidebar col-sm-3 col-md-2 affix" style="height: 100%;" ng-controller="sidebarCtrl">
                <li ng-repeat="item in sidebar track by $index" ng-class="{'active': item.active==1}"><a ng-href="{{item.url}}">{{item.label}}</a>
                </li>
            </ul>
        </div>
        <div class="col-sm-9 col-md-10">
            <p class="visible-xs">
                <button type="button" class="btn btn-primary btn-sm off-canvas-toggle" data-toggle="offcanvas">
                    <i class="glyphicon glyphicon-chevron-left"></i>
                </button>
            </p>
            <h1 class="page-header text-muted">Configuración de galería</h1>
            <div class="row placeholders">
                <div class="col-md-12 placeholder">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4>
                                    <i class="fa fa-list-ul pull-right"></i> Gestión de imágenes de galería
                                </h4>
                            </div>
                        </div>
                        <div class="panel-body">Lista de imágenes</div>
                        <p>
                            <span class="label label-default"> Galería </span>
                        </p>
                        <div>
                            <?php foreach ($css_files as $file) : ?>
                                <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
                            <?php endforeach; ?>
                            <?php foreach ($js_files as $file) : ?>
                                <script src="<?php echo $file; ?>"></script>
                            <?php endforeach; ?>
                            <?php echo $output; ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2 alert" ng-class="popup.type" ng-show="popup.show" style="position: fixed; bottom: 0px; left: 0px;">
        <a class="close" ng-click="popup.show = false"><sup>&times;</sup></a>
        <label>{{popup.title}}</label>
        <p>{{popup.message}}</p>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $(".select-autocomplete").select2({
            placeholder: "Seleccione uno",
            allowClear: true
        });
        $('#editor').wysiwyg();
        $('#editor').cleanHtml();
    });
    $('.off-canvas-toggle').on('click', function(event) {
        event.preventDefault();
        $('.row-offcanvas-left').toggleClass('active');
    });
    angular
        .module("mainApp", [])
        .factory("dataFactory", ['$http', '$q', '$location',
            function($http, $q, $location) {
                var dataBaseUrl = "<?php echo base_url(); ?>";
                var objFactory = {};
                objFactory.get = function(q) {
                    return $http.get(dataBaseUrl + q).then(function(data, status, headers, config) {
                        return data.data;
                    }, function(data, status, headers, config) {
                        console.log('[GET] ocurrió un error en ' + dataBaseUrl + q, data);
                    });
                };
                objFactory.post = function(q, object) {
                    return $http.post(dataBaseUrl + q, object).then(function(data, status, headers, config) {
                        return data.data;
                    }, function(data, status, headers, config) {
                        console.log('[POST] ocurrió un error en ' + dataBaseUrl + q, data);
                        console.log(object);
                    });
                };
                return objFactory;
            }
        ])
        .controller("sidebarCtrl", function($scope, dataFactory) {
            dataFactory.get('admin/sidebar/3').then(function(data, status, headers, config) {
                $scope.sidebar = data;
            });
        })
        .controller("exalumnoCtrl", function($scope, $sce, dataFactory) {
            $scope.popup = {
                type: 'alert-info',
                show: false,
                title: 'Información',
                message: 'Descripción de la información'
            };
            $scope.load = function() {
                dataFactory.get('admin/data_exalumno').then(function(data, status, headers, config) {
                    $scope.exalumnos = data;
                });
            };
            $scope.load();
        })
        .controller("socialCtrl", function($scope, $sce, dataFactory) {
            $scope.category = 'facebook_link';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.facebook_link = data.value;
            });
            $scope.category = 'twitter_link';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.twitter_link = data.value;
            });
            $scope.category = 'youtube_link';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.youtube_link = data.value;
            });
        });
</script>