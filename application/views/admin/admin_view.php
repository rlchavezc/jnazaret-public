<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="container-fluid" ng-controller="contentCtrl">
    <div class="row row-offcanvas row-offcanvas-left">
        <div class="col-sm-3 col-md-2 sidebar-offcanvas" role="navigation">
            <ul class="nav nav-sidebar col-sm-3 col-md-2 affix" style="height: 100%;" ng-controller="sidebarCtrl">
                <li ng-repeat="item in sidebar track by $index" ng-class="{'active': item.active==1}"><a ng-href="{{item.url}}">{{item.label}}</a>
                </li>
            </ul>
        </div>
        <div class="col-sm-9 col-md-10">
            <p class="visible-xs">
                <button type="button" class="btn btn-primary btn-sm off-canvas-toggle" data-toggle="offcanvas">
                    <i class="glyphicon glyphicon-chevron-left"></i>
                </button>
            </p>
            <h1 class="page-header text-muted">Lista de contenidos</h1>
            <div class="row placeholders">
                <div class="col-md-12 placeholder">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4>
                                    <i class="fa fa-list-ul pull-right"></i> Resumen de contenidos
                                </h4>
                            </div>
                        </div>
                        <div class="panel-body">Lista de contenidos</div>
                        <p>
                            <span class="label label-default"> Contenidos </span>
                        </p>
                        <div class="controls">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                <input class="form-control" id="searchContent" placeholder="Filtrar contenidos" ng-model="searchContent" type="text" /> <span class="input-group-addon" ng-click="searchContent = ''"><i class="fa fa-eraser"></i>
                                    Borrar</span>
                            </div>
                        </div>
                        <hr />
                        <div class="table-responsive">
                            <table class="table table-condensed table-hover">
                                <thead>
                                    <tr class="info">
                                        <th><a href="" ng-click="sortContentType = 'id'; sortContentReverse = !sortContentReverse">
                                                # </a></th>
                                        <th><a href="" ng-click="sortContentType = 'title'; sortContentReverse = !sortContentReverse">
                                                <i class="fa fa-tag"></i> Título
                                            </a></th>
                                        <th><a href="" ng-click="sortContentType = 'value'; sortContentReverse = !sortContentReverse">
                                                Contenido </a></th>
                                        <th><a href="" ng-click="sortContentType = 'status_name'; sortContentReverse = !sortContentReverse">
                                                Editar </a></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td colspan="8" align="right">
                                            <div class="table-responsive">
                                                <table class="table table-condensed table-hover">
                                                    <thead>
                                                        <tr class="info">
                                                            <th ng-repeat="item in orderChart">{{item.label}}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td ng-repeat="item in orderChart">{{item.value}}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <tr ng-repeat="item in contents | orderBy:sortContentType:sortContentReverse | filter: searchContent" ng-class="item.class_name">
                                        <td>{{item.id}}</td>
                                        <td>{{item.title}}</td>
                                        <td ng-bind-html="item.showValue"></td>
                                        <td>
                                            <button class='btn btn-default' data-toggle='modal' data-target='#editModal' ng-click="edit(item.category)">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4>Editar Contenido</h4>
                </div>
                <div class="modal-body">
                    <div class="control-group">
                        <label for="name_cutomer">Título</label>
                        <div class="controls">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input class="form-control" type="text" name="name_customer_edit" id="name_customer_edit" ng-model="editContent.title" />
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="email_cutomer">Contenido</label>
                        <div class="controls">
                            <div class="input-group" ng-show="false">
                                <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                <textarea id="edit_content_value" rows="8" cols="30" class="form-control" ng-model="editContent.value"></textarea>
                            </div>
                            <?php $this->load->view('common/rich_text_editor_common_view', array('id' => 'editor')); ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="update()">Actualizar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2 alert" ng-class="popup.type" ng-show="popup.show" style="position: fixed; bottom: 0px; left: 0px;">
        <a class="close" ng-click="popup.show = false"><sup>&times;</sup></a>
        <label>{{popup.title}}</label>
        <p>{{popup.message}}</p>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $(".select-autocomplete").select2({
            placeholder: "Seleccione uno",
            allowClear: true
        });
        $('#editor').wysiwyg();
        $('#editor').cleanHtml();
    });
    $('.off-canvas-toggle').on('click', function(event) {
        event.preventDefault();
        $('.row-offcanvas-left').toggleClass('active');
    });
    angular
        .module("mainApp", [])
        .factory("dataFactory", ['$http', '$q', '$location',
            function($http, $q, $location) {
                var dataBaseUrl = "<?php echo base_url(); ?>";
                var objFactory = {};
                objFactory.get = function(q) {
                    return $http.get(dataBaseUrl + q).then(function(data, status, headers, config) {
                        return data.data;
                    }, function(data, status, headers, config) {
                        console.log('[GET] ocurrió un error en ' + dataBaseUrl + q, data);
                    });
                };
                objFactory.post = function(q, object) {
                    return $http.post(dataBaseUrl + q, object).then(function(data, status, headers, config) {
                        return data.data;
                    }, function(data, status, headers, config) {
                        console.log('[POST] ocurrió un error en ' + dataBaseUrl + q, data);
                        console.log(object);
                    });
                };
                return objFactory;
            }
        ])
        .controller("sidebarCtrl", function($scope, dataFactory) {
            dataFactory.get('admin/sidebar').then(function(data, status, headers, config) {
                $scope.sidebar = data;
            });
        })
        .controller("contentCtrl", function($scope, $sce, dataFactory) {
            $scope.popup = {
                type: 'alert-info',
                show: false,
                title: 'Información',
                message: 'Descripción de la información'
            };
            $scope.load = function() {
                dataFactory.get('admin/data_content').then(function(data, status, headers, config) {
                    $scope.contents = data;
                    for (var i = 0; i < $scope.contents.length; i++)
                        $scope.contents[i].showValue = $sce.trustAsHtml($scope.contents[i].value);
                });
            };
            $scope.edit = function(category) {
                dataFactory.get('admin/data_content/' + category).then(function(data, status, headers, config) {
                    $scope.editContent = data;
                    $("#editor").html($scope.editContent.value);
                });
            };
            $scope.update = function() {
                $scope.editContent.value = $("#editor").html();
                dataFactory.post('admin/update_content', $scope.editContent).then(function(data, status, headers, config) {
                    $scope.popup.show = true;
                    $scope.popup.title = "Operación exitosa";
                    $scope.popup.message = "Se actualizó la información de " + data.title;
                    $scope.popup.type = "alert-success";
                    $scope.load();
                });
            };
            $scope.load();
        })
        .controller("socialCtrl", function($scope, $sce, dataFactory) {
            $scope.category = 'facebook_link';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.facebook_link = data.value;
            });
            $scope.category = 'twitter_link';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.twitter_link = data.value;
            });
            $scope.category = 'youtube_link';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.youtube_link = data.value;
            });
        });
</script>