<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="container-fluid" ng-controller="contactCtrl">
    <div class="row row-offcanvas row-offcanvas-left">
        <div class="col-sm-3 col-md-2 sidebar-offcanvas" role="navigation">
            <ul class="nav nav-sidebar col-sm-3 col-md-2 affix" style="height: 100%;" ng-controller="sidebarCtrl">
                <li ng-repeat="item in sidebar track by $index" ng-class="{'active': item.active==1}"><a ng-href="{{item.url}}">{{item.label}}</a>
                </li>
            </ul>
        </div>
        <div class="col-sm-9 col-md-10">
            <p class="visible-xs">
                <button type="button" class="btn btn-primary btn-sm off-canvas-toggle" data-toggle="offcanvas">
                    <i class="glyphicon glyphicon-chevron-left"></i>
                </button>
            </p>
            <h1 class="page-header text-muted">Lista de contactos</h1>
            <div class="row placeholders">
                <div class="col-md-12 placeholder">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4>
                                    <i class="fa fa-list-ul pull-right"></i> Resumen de contactos
                                </h4>
                            </div>
                        </div>
                        <div class="panel-body">Lista de contactos</div>
                        <p>
                            <span class="label label-default"> Contactos </span>
                        </p>
                        <div class="controls">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                <input class="form-control" id="searchContact" placeholder="Filtrar contactos" ng-model="searchContact" type="text" /> <span class="input-group-addon" ng-click="searchContact = ''"><i class="fa fa-eraser"></i>
                                    Borrar</span>
                            </div>
                        </div>
                        <hr />
                        <div class="table-responsive">
                            <table class="table table-condensed table-hover">
                                <thead>
                                    <tr class="info">
                                        <th>
                                            <a href="" ng-click="sortContactType = 'id'; sortContactReverse = !sortContactReverse">
                                                # </a>
                                        </th>
                                        <th>
                                            <a href="" ng-click="sortContactType = 'name'; sortContactReverse = !sortContactReverse">
                                                <i class="fa fa-tag"></i> Nombre
                                            </a>
                                        </th>
                                        <th>
                                            <a href="" ng-click="sortContactType = 'phone'; sortContactReverse = !sortContactReverse">
                                                Teléfono
                                            </a>
                                        </th>
                                        <th>
                                            <a href="" ng-click="sortContactType = 'email'; sortContactReverse = !sortContactReverse">
                                                Correo
                                            </a>
                                        </th>
                                        <th>
                                            <a href="" ng-click="sortContactType = 'comment'; sortContactReverse = !sortContactReverse">
                                                Comentario
                                            </a>
                                        </th>
                                        <th>
                                            <a href="" ng-click="sortContactType = 'date_insert'; sortContactReverse = !sortContactReverse">
                                                Fecha de inserción
                                            </a>
                                        </th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td colspan="6" align="right">
                                            <div class="table-responsive">
                                                <table class="table table-condensed table-hover">
                                                    <thead>
                                                        <tr class="info">
                                                            <th ng-repeat="item in orderChart">{{item.label}}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td ng-repeat="item in orderChart">{{item.value}}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <tr ng-repeat="item in contacts | orderBy:sortContactType:sortContactReverse | filter: searchContact" ng-class="item.class_name">
                                        <td>{{item.id}}</td>
                                        <td>{{item.name}}</td>
                                        <td>{{item.phone}}</td>
                                        <td><a href="mailto:{{item.email}}">{{item.email}}</a></td>
                                        <td>{{item.comment}}</td>
                                        <td>{{item.date_insert}}</td>
                                        <td><button class="btn btn-danger btn-xs" data-toggle='modal' data-target='#confirmModal' title="Borrar Contacto" ng-click="prev(item)"><i class="fa fa-trash"></i></button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2 alert" ng-class="popup.type" ng-show="popup.show" style="position: fixed; bottom: 0px; left: 0px;">
        <a class="close" ng-click="popup.show = false"><sup>&times;</sup></a>
        <label>{{popup.title}}</label>
        <p>{{popup.message}}</p>
    </div>
    <div class="modal fade" id="confirmModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4><i class="fa fa-history"></i> Eliminar registro </h4>
                </div>
                <div class="modal-body">
                    <p>¿Está seguro de eliminar el registro?</p>
                    <div class="btn-group">
                        <button class="btn btn-primary" data-dismiss="modal" ng-click="del(delete_contact.id)"><i class="fa fa-trash"></i> Sí</button>
                        <button class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $(".select-autocomplete").select2({
            placeholder: "Seleccione uno",
            allowClear: true
        });
        $('#editor').wysiwyg();
        $('#editor').cleanHtml();
    });
    $('.off-canvas-toggle').on('click', function(event) {
        event.preventDefault();
        $('.row-offcanvas-left').toggleClass('active');
    });
    angular
        .module("mainApp", [])
        .factory("dataFactory", ['$http', '$q', '$location',
            function($http, $q, $location) {
                var dataBaseUrl = "<?php echo base_url(); ?>";
                var objFactory = {};
                objFactory.get = function(q) {
                    return $http.get(dataBaseUrl + q).then(function(data, status, headers, config) {
                        return data.data;
                    }, function(data, status, headers, config) {
                        console.log('[GET] ocurrió un error en ' + dataBaseUrl + q, data);
                    });
                };
                objFactory.post = function(q, object) {
                    return $http.post(dataBaseUrl + q, object).then(function(data, status, headers, config) {
                        return data.data;
                    }, function(data, status, headers, config) {
                        console.log('[POST] ocurrió un error en ' + dataBaseUrl + q, data);
                        console.log(object);
                    });
                };
                return objFactory;
            }
        ])
        .controller("sidebarCtrl", function($scope, dataFactory) {
            dataFactory.get('admin/sidebar/1').then(function(data, status, headers, config) {
                $scope.sidebar = data;
            });
        })
        .controller("contactCtrl", function($scope, $sce, dataFactory) {
            $scope.popup = {
                type: 'alert-info',
                show: false,
                title: 'Información',
                message: 'Descripción de la información'
            };
            $scope.load = function() {
                dataFactory.get('admin/data_contact').then(function(data, status, headers, config) {
                    $scope.contacts = data;
                });
            };
            $scope.del = function(id) {
                dataFactory.get('admin/delete_contact/' + id).then(function(data, status, headers, config) {
                    $scope.load();
                });
            };
            $scope.prev = function(item) {
                $scope.delete_contact = item;
            };
            $scope.load();
        })
        .controller("socialCtrl", function($scope, $sce, dataFactory) {
            $scope.category = 'facebook_link';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.facebook_link = data.value;
            });
            $scope.category = 'twitter_link';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.twitter_link = data.value;
            });
            $scope.category = 'youtube_link';
            dataFactory.get('home/data_content/' + $scope.category).then(function(data, status, headers, config) {
                $scope.youtube_link = data.value;
            });
        });
</script>