<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!doctype html>
<html lang="es" data-ng-app="mainApp">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <meta name="description" content="Colegio Privado Jesús de Nazareth" />
    <meta name="theme-color" content="#337ab7" />
    <title><?php echo $title ?></title>
    <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" /> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/justified-nav.css" />
    <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/cosmo/bootstrap.css" /> -->
    <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-theme.min.css" /> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="https://bootswatch.com/3/readable/bootstrap.min.css" /> -->
    <!-- <link rel="stylesheet" href="https://bootswatch.com/3/lumen/bootstrap.min.css" /> -->
    <!-- <link rel="stylesheet" href="https://bootswatch.com/3/flatly/bootstrap.min.css" /> -->
    <!-- <link rel="stylesheet" href="https://bootswatch.com/3/simplex/bootstrap.min.css" /> -->
    <!-- <link rel="stylesheet" href="https://bootswatch.com/3/spacelab/bootstrap.min.css" /> -->
    <!-- <link rel="stylesheet" href="https://bootswatch.com/3/paper/bootstrap.min.css" /> -->
    <!-- <link rel="stylesheet" href="https://bootswatch.com/3/cosmo/bootstrap.min.css" /> -->
    <link rel="preload" as="font" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.woff2" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha512-SfTiTlX6kk+qitfevl/7LibUOeJWlt9rbyDn92a1DqWOw9vWG2MFoays0sgObmWazO5BQPiFucnnEAjpAB+/Sw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css" /> -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/select2.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css" />
    <link rel="manifest" href="/manifest.json">
    <link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/img/escudo.png">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script> -->
    <!-- <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script> -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-3.3.2.js"></script>
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/select2.full.min.js"></script>
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/angular.min.js"></script> -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.8.2/angular.min.js"></script>
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Chart.min.js"></script> -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.hotkeys.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-wysiwyg.js"></script>
    <!-- <script type="text/javascript">
		window.smartlook||(function(d) {
			var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
			var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
			c.charset='utf-8';c.src='//rec.getsmartlook.com/recorder.js';h.appendChild(c);
		})(document);
		smartlook('init', 'ea85d357163f7f07290207681df1e467e3242e2b');
	</script> -->
    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NB6KDC');
    </script>
    <!-- End Google Tag Manager -->
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/chartist.js"></script> -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?php echo base_url(); ?>assets/js/html5.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/respond.min.js"></script>
    <![endif]-->
    <script type='application/ld+json'>
        {
            "@context": "http://www.schema.org",
            "@type": "WebSite",
            "name": "Colegio Privado Jesús de Nazareth",
            "alternateName": "Colegio Jesús de Nazareth",
            "url": "https://www.jnazareth.edu.pe/main"
        }
    </script>
    <script type='text/javascript'>
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker
                .register('/sw.js?v=1.0.0')
                .then(function() {
                    console.log("Service Worker Registered");
                });
        }
    </script>
</head>

<body class="bg-custom-1">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe title="tag-manager" src="https://www.googletagmanager.com/ns.html?id=GTM-NB6KDC" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->