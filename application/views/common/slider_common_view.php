<?php
defined('BASEPATH') or exit('No direct script access allowed');
$size = isset($size) ? $size : 'col-md-offset-3 col-md-6';
?>
<!-- 
Needed:
	$id = id_element{
	$ng_controller= carousel name controller
-->
<br />
<div id="<?php echo $id; ?>" class="carousel slide <?php echo $size; ?>" ng-controller="<?php echo $ng_controller; ?>" data-interval="8000">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li ng-repeat="item in carousels" data-target="<?php echo "#" . $id; ?>" data-slide-to="{{$index}}" ng-class="$index==0? 'active' : ''"></li>
    </ol>
    <div class="carousel-inner" style="background: url('<?php echo base_url(); ?>assets/img/escudo.png') no-repeat scroll center center;">
        <!-- class active -->
        <div ng-repeat="item in carousels" class="item" ng-class="$index==0? 'active' : ''">
            <img ng-src="{{item.img}}" class="img-responsive" alt="{{item.img}}" />
            <div class="container">
                <div class="carousel-caption">
                    <h3>{{item.title}}</h3>
                    <p>
                        {{item.content}}<br />
                        <a class="btn btn-sm btn-primary" ng-href="{{item.link}}">Más información >> {{item.title}}</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- Controls -->
    <a class="left carousel-control" href="<?php echo "#" . $id; ?>" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="<?php echo "#" . $id; ?>" data-slide="next">
        <span class="icon-next"></span>
    </a>
</div>
<div class="clearfix"></div>
<style>
    .carousel-inner>.active {
        opacity: 0.90;
    }
</style>
<script type="text/javascript">
    setInterval(function() {
        $("#<?php echo $id; ?>").carousel("next");
    }, 8000);
</script>
<hr />