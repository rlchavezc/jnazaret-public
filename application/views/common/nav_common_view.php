<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!-- Header -->
<nav id="top-nav" class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><?php echo $title; ?></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#">
                        <i class="glyphicon glyphicon-user" aria-hidden="true"></i> <?php echo $title; ?> <span class="caret"></span>
                    </a>
                    <ul id="g-account-menu" class="dropdown-menu" role="menu">
                        <li><a href="#">Mi Perfil</a></li>
                        <li><a href="#">Acerca de ...</a></li>
                    </ul>
                </li>
                <li><a href="<?php echo base_url(); ?>login/logout"><i class="glyphicon glyphicon-lock" aria-hidden="true"></i> Salir</a></li>
            </ul>
        </div>
    </div><!-- /container -->
</nav>
<style>
    body {
        padding-top: 50px;
    }

    .main {
        padding: 20px;
    }

    footer {
        padding-left: 15px;
        padding-right: 15px;
    }

    @media screen and (max-width:768px) {
        .row-offcanvas {
            position: relative;
            transition: all 0.25s ease-out;
        }

        .row-offcanvas-left .sidebar-offcanvas {
            left: -33%;
        }

        .row-offcanvas-left.active {
            left: 33%;
        }

        .side-offcanvas {
            posittion: absolute;
            top: 0;
            width: 33%;
            margin-left: 10px;
        }

        .affix {
            position: fixed;
        }
    }

    .nav-sidebar {
        background-color: #f5f5f5;
        margin-right: -15px;
        margin-bottom: 20px;
        margin-left: -15px;
    }

    .nav-sidebar>li>a {
        padding-right: 20px;
        padding-left: 20px;
    }

    .nav-sidebar>.active>a {
        color: #fff;
        background-color: #428bca;
    }

    .label-form {
        min-width: 105px;
    }
</style>
<?php
    if ($this->session->has_userdata('name')) {
?>
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        document.querySelectorAll("footer").forEach((el) => {
            el.classList.add('col-md-offset-2');
        });
    });
</script>
<?php
    }
?>
<!-- /Header -->