<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<hr />
<div class="clearfix"></div>
<footer>
    <section>
        <?php $this->load->view('home/common/addresses_view'); ?>
        <address class="col-md-6" ng-controller="socialCtrl">
            <div class="pull-right">
                <a class="btn btn-sm btn-primary" ng-href="{{facebook_link}}" target="_blank" rel="noreferrer">
                    <i class="fa fa-facebook fa-lg" aria-hidden="true"></i> Facebook
                </a>
                <a class="btn btn-sm btn-info" ng-href="{{twitter_link}}" target="_blank" rel="noreferrer" style="display:none">
                    <i class="fa fa-twitter fa-lg" aria-hidden="true"></i> Twitter
                </a>
                <a class="btn btn-sm btn-danger" ng-href="{{youtube_link}}" target="_blank" rel="noreferrer" style="display:none">
                    <i class="fa fa-youtube fa-lg" aria-hidden="true"></i> Youtube
                </a>
                <br />
                <br />
                <p class="text-right">
                    <i class="fa fa-copyright" aria-hidden="true"></i> Todos los derechos reservados
                </p>
            </div>
        </address>
    </section>
</footer>

</body>

</html>