<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Contact_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function get_contact($id = FALSE, $order = 'DESC')
    {
        if ($id === FALSE) {
            $this->db->order_by('id', $order);
            $query = $this->db->get('contact');
            return $query->result_array();
        }
        $query = $this->db->get_where('contact', array(
            'id' => $id
        ));
        return $query->row_array();
    }
    public function add_contact($data)
    {
        $this->db->insert('contact', $data);
        return $this->db->affected_rows();
    }
    public function update_contact($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('contact', $data);
        return $this->get_contact($id);
    }
    public function delete_contact($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('contact');
        return true;
    }
}
