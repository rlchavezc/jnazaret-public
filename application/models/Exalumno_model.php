<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Exalumno_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function get_exalumno($id = FALSE, $order = 'DESC')
    {
        if ($id === FALSE) {
            $this->db->order_by('id', $order);
            $query = $this->db->get('exalumno');
            return $query->result_array();
        }
        $query = $this->db->get_where('exalumno', array(
            'id' => $id
        ));
        return $query->row_array();
    }
    public function add_exalumno($data)
    {
        $this->db->insert('exalumno', $data);
        return $this->db->affected_rows();
    }
    public function update_exalumno($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('exalumno', $data);
        return $this->get_exalumno($id);
    }
    public function delete_exalumnos($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('exalumno');
        return true;
    }
}
