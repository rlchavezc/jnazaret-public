<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class User_model extends CI_Model {
	public function __construct() {
		parent::__construct ();
		$this->load->database();
	}
	public function get_users($id = FALSE) {
		if ($id === FALSE) {
			$query = $this->db->get ( 'user' );
			return $query->result_array ();
		}
		$query = $this->db->get_where ( 'user', array (
				'id' => $id 
		) );
		return $query->row_array ();
	}
	public function login($user, $pass){
		$query = $this->db->get_where ( 'user', array (
				'user' => $user,
				'pass' => $pass 
		) );
		return $query->row_array ();
	}
}