<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Ubigeo_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function get_ubigeo($id = FALSE, $order = 'DESC')
    {
        if ($id === FALSE) {
            $this->db->order_by('id', $order);
            return $this->db->get('ubigeo')->result_array();
        }
        return $this->db->get_where('ubigeo', array(
            'id' => $id
        ))->row_array();
    }
    public function get_departamentos()
    {
        $this->db->select('departamento');
        $this->db->distinct();
        $this->db->group_by('departamento');
        return $this->db->get('ubigeo')->result_array();
    }
    public function get_provincias($departamento)
    {
        $this->db->select('provincia');
        $this->db->distinct();
        $this->db->group_by('provincia');
        return $this->db->get_where('ubigeo', array(
            'departamento' => $departamento
        ))->result_array();
    }
    public function get_distritos($departamento, $provincia)
    {
        $this->db->select('distrito');
        $this->db->distinct();
        $this->db->group_by('distrito');
        return $this->db->get_where('ubigeo', array(
            'departamento' => $departamento,
            'provincia' => $provincia
        ))->result_array();
    }
    public function add_ubigeo($data)
    {
        $this->db->insert('ubigeo', $data);
        return $this->db->insert_id();
    }
    public function update_ubigeo($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('ubigeo', $data);
        return $this->get_ubigeo($id);
    }
    public function delete_ubigeo($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('ubigeo');
        return true;
    }
}
