<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Conciliacion_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function get_conciliacion($id = FALSE, $order = 'DESC')
    {
        $this->load->helper('util_helper');
        if ($id === FALSE) {
            $this->db->order_by('id', $order);
            $result = $this->db->get('conciliacion')->result_array();
            $resultFilled = [];
            foreach ($result as $item) {
                $item['foto1_url'] = isset($item['foto1']) ? conciliacion_location_file('foto1', $item['foto1'], true) : "";
                $item['foto2_url'] = isset($item['foto2']) ? conciliacion_location_file('foto2', $item['foto2'], true) : "";
                $item['firma_url'] = isset($item['firma']) ? conciliacion_location_file('firma', $item['firma'], true) : "";
                $resultFilled[] = $item;
            }
            return $resultFilled;
        }
        $result = $this->db->get_where('conciliacion', array(
            'id' => $id
        ))->row_array();
        $result['foto1_url'] = isset($result['foto1']) ? conciliacion_location_file('foto1', $result['foto1'], true) : "";
        $result['foto2_url'] = isset($result['foto2']) ? conciliacion_location_file('foto2', $result['foto2'], true) : "";
        $result['firma_url'] = isset($result['firma']) ? conciliacion_location_file('firma', $result['firma'], true) : "";
        return $result;
    }
    public function get_conciliacion_by_id_reclamo($id_reclamo)
    {
        $this->load->helper('util_helper');
        $result = $this->db->get_where('conciliacion', array(
            'id_reclamo' => $id_reclamo
        ))->row_array();
        $result['foto1_url'] = isset($result['foto1']) ? conciliacion_location_file('foto1', $result['foto1'], true) : "";
        $result['foto2_url'] = isset($result['foto2']) ? conciliacion_location_file('foto2', $result['foto2'], true) : "";
        $result['firma_url'] = isset($result['firma']) ? conciliacion_location_file('firma', $result['firma'], true) : "";
        return $result;
    }
    public function add_conciliacion($data)
    {
        $this->db->insert('conciliacion', $data);
        return $this->db->insert_id();
    }
    public function update_conciliacion($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('conciliacion', $data);
        return $this->get_conciliacion($id);
    }
    public function delete_conciliacion($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('conciliacion');
        return true;
    }
}
