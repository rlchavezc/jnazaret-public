<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Content_model extends CI_Model {
	public function __construct() {
		parent::__construct ();
		$this->load->database();
	}
	public function get_content($category = FALSE) {
		if ($category === FALSE) {
			$query = $this->db->get ( 'content' );
			return $query->result_array ();
		}
		$query = $this->db->get_where ( 'content', array (
			'category' => $category 
		) );
		return $query->row_array ();
	}
	public function update_content($category, $data) {
		$this->db->where ( 'category', $category );
		$this->db->update ( 'content', $data );
		$data = $this->get_content($category);
		return $data;
	}
}