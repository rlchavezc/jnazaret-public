<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Libroreclamacion_model extends CI_Model
{
    public const REGISTRADO = 'Registrado';
    public const CONCILIADO = 'Conciliado';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function get_libro_reclamacion($id = FALSE, $order = 'DESC')
    {
        $this->load->helper('util_helper');
        if ($id === FALSE) {
            $this->db->order_by('id', $order);
            $result = $this->db->get('libro_reclamacion')->result_array();
            $resultFilled = [];
            foreach ($result as $item) {
                if ($item['estado'] == self::REGISTRADO) {
                    $item['class_name'] = 'warning';
                }
                if ($item['estado'] == self::CONCILIADO) {
                    $item['class_name'] = 'success';
                    $this->load->model('conciliacion_model');
                    $conciliacion = $this->conciliacion_model->get_conciliacion_by_id_reclamo($item['id']);
                    $conciliacion = isset($conciliacion[0]) ? $conciliacion[0] : $conciliacion;
                    $item['conciliacion'] = $conciliacion;
                }
                $item['adjunto1_url'] = isset($item['adjunto1']) ? conciliacion_location_file('adjunto1', $item['adjunto1'], true) : "";
                $item['adjunto2_url'] = isset($item['adjunto2']) ? conciliacion_location_file('adjunto2', $item['adjunto2'], true) : "";
                $resultFilled[] = $item;
            }
            return $resultFilled;
        }
        $result = $this->db->get_where('libro_reclamacion', array(
            'id' => $id
        ))->row_array();
        $result['adjunto1_url'] = isset($result['adjunto1']) ? conciliacion_location_file('adjunto1', $result['adjunto1'], true) : "";
        $result['adjunto2_url'] = isset($result['adjunto2']) ? conciliacion_location_file('adjunto2', $result['adjunto2'], true) : "";
        return $result;
    }
    public function add_libro_reclamacion($data)
    {
        $this->db->insert('libro_reclamacion', $data);
        return $this->db->insert_id();
    }
    public function update_libro_reclamacion($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('libro_reclamacion', $data);
        return $this->get_libro_reclamacion($id);
    }
    public function delete_libro_reclamacion($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('libro_reclamacion');
        return true;
    }
}
