<?php

// Create this file.
// file: application/helpers/util_helper.php

if (!function_exists('conciliacion_location_file')) {
    /**
     * Return the default value of the given value.
     *
     * @param mixed $value
     *
     * @return mixed
     */
    function conciliacion_location_file($type, $file_name = "", $url = false)
    {
        $location = "assets/uploads/libro_reclamacion/{$type}/{$file_name}";
        if(!$url){
            return "./{$location}";
        }
        return base_url()."{$location}";
    }
}
