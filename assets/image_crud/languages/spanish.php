<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['upload_button'] = 'Cargar archivos aquí';
$lang['upload-drop-area'] = 'Borrar archivos aquí';
$lang['upload-cancel'] = 'Cancelar';
$lang['upload-failed'] = 'Ocurrió un error';

$lang['loading'] = 'Cargando, espere por favor...';
$lang['deleting'] = 'Borrando, espere por favor...';
$lang['saving_title'] = 'Guardando título...';

$lang['list_delete'] = 'Borrar';
$lang['alert_delete'] = '¿Seguro que quiere borrar esta imagen?';

/* End of file spanish.php */
/* Location: ./assets/image_crud/languages/spanish.php */